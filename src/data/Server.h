//
// Created by duso on 22-Dec-16.
//

#ifndef PONG_UDP_SERVER_H
#define PONG_UDP_SERVER_H

#include <string>
#include <SFML/Network/IpAddress.hpp>
#include "../main/Message.h"

class Server
{
public:
    Server(){

    }

    Server(sf::IpAddress address, unsigned short port, std::string id, std::string name)
    {
        construct(address, port, id, name);
    }

    Server(Message &message)
    {
        if (message.getType() != SERVER_BROADCAST_INVITE) {
            throw "Tried to parse invalid message type";
        }
        Json json = message.getJson();

        typedef MessageJsonFields fields; // shorten the name of enum

        construct(message.addr, message.port, json[fields::ID], json[fields::GAME_NAME]);
    }

    sf::IpAddress address;
    unsigned short port;
    std::string id;
    std::string name;
    float age = 0;

private:
    void construct(sf::IpAddress address, unsigned short port, std::string id, std::string name){
        this->address = address;
        this->port = port;
        this->id = id;
        this->name = name;
        this->age = 0;
    }
};


#endif //PONG_UDP_SERVER_H
