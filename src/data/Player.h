//
// Created by duso on 23-Dec-16.
//

#ifndef PONG_UDP_PLAYER_H
#define PONG_UDP_PLAYER_H


#include "../main/Message.h"

class Player
{
public:
    Player()
    {
        this->age = 0;
        this->is_server = false;
    };

    Player(Json &json)
    {
        typedef MessageJsonFields f;

        this->name = json[f::PLAYER_NAME];
        this->color = json[f::COLOR];
        this->age = 0;
        this->is_server = false;
    }

    Player(Message &message)
    {
        Json json = message.getJson();

        typedef MessageJsonFields f;

        this->address = message.addr;
        this->port = message.port;
        this->id = json[f::ID];
        this->name = json[f::PLAYER_NAME];
        this->color = json[f::COLOR];
        this->frame = json[f::FRAME];
        this->age = 0;
        this->is_server = false;
    }

    sf::IpAddress address;
    unsigned short port;

    std::string id;
    std::string name;
    std::string color;
    bool is_server = false;

    int frame;
    float age = 0; // kedy som naposledy od neho nieco pocul
    float contact = 0; // kedy som naposledy ja mu nieco poslal

private:

};


#endif //PONG_UDP_PLAYER_H
