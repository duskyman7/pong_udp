//
// Created by Duso on 10. 9. 2016.
//

#ifndef PONG_UDP_LOBBY_H
#define PONG_UDP_LOBBY_H


#include <SFML/Graphics/Text.hpp>
#include "../states/State.h"
#include "../main/Resources.h"
#include "../data/Player.h"

class Lobby : public State
{
public:
    Lobby(StateManager *pManager) : State(pManager)
    {
        std::cout << "Lobby::Lobby()" << std::endl;

        text_client.setCharacterSize(20);
        text_client.setFillColor(sf::Color::Yellow);
        text_client.setFont(Resources::getFont(FONT1));

        title.setCharacterSize(20);
        title.setFillColor(sf::Color::Yellow);
        title.setFont(Resources::getFont(FONT1));
    }

    virtual void render() override;

protected:
    // GUI
    sf::Text title;
    sf::Text text_client;
};


#endif //PONG_UDP_LOBBY_H
