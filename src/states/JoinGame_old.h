//
// Created by Duso on 24. 8. 2016.
//

#ifndef PONG_UDP_JOINGAME_H
#define PONG_UDP_JOINGAME_H


#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/System/Clock.hpp>
#include "State.h"
#include "../main/Network.h"
#include "../main/Message.h"
#include "../main/Resources.h"
#include "../main/Host.h"

class JoinGame: public State {
public:
    JoinGame(StateManager *sm) : State(sm) {
        text_host.setFont(Resources::getFont(FONT1));
        text_host.setFillColor(sf::Color::Yellow);
        text_host.setCharacterSize(20);

        text.setString("JoinGame");
        text.setCharacterSize(20);
        text.setFillColor(sf::Color::Yellow);
        text.setFont(Resources::getFont(FONT1));
        text.setPosition(atoi(Resources::getGameData(WINDOW_WIDTH).c_str())/2 - text.getGlobalBounds().width/2,
                         10);

        text_manual_label.setString("(m) Connect manually");
        text_manual_label.setCharacterSize(20);
        text_manual_label.setFillColor(sf::Color::Yellow);
        text_manual_label.setFont(Resources::getFont(FONT1));
        text_manual_label.setPosition(atoi(Resources::getGameData(WINDOW_WIDTH).c_str())/2 - text_manual_label.getGlobalBounds().width/2,
                                atoi(Resources::getGameData(WINDOW_HEIGHT).c_str()) - text_manual_label.getGlobalBounds().height - 10);

        text_manual.setCharacterSize(20);
        text_manual.setFillColor(sf::Color::Yellow);
        text_manual.setFont(Resources::getFont(FONT1));

        rect.setFillColor(sf::Color(32,32,32));
        rect.setOutlineColor(sf::Color(16,16,16));
        rect.setOutlineThickness(2);
    }

    virtual void input() override;

    virtual void update(float time) override;

    virtual void render() override;

    virtual void load() override;

private:

    Message msg;
    std::vector<Host> servers;
    Host *chosen_host;
    sf::Clock join_message_timer;
    unsigned int join_retry_counter;
    Message join_message;

    sf::Text text;
    sf::Text text_host;
    sf::Text text_manual_label;
    sf::Text text_manual;
    sf::RectangleShape rect;

    bool show_manual = false;

    void removeOldHosts();

    void handleNetworkMessages();

    void checkNumbers();

    void tryJoinHost(Host &server);

    void handleInvitation(Message &message);

    void tryGoToLobby(Message &message);

    void retryJoinHost();

    bool input_manual();

    bool input_main();

    void manualJoinHost();
};


#endif //PONG_UDP_JOINGAME_H
