//
// Created by Duso on 23. 8. 2016.
//

#include <iostream>
#include "State.h"

void State::input() {
    std::cout << "State::input()" << std::endl;
}

void State::update(float time) {
    std::cout << "State::update()" << std::endl;
}

void State::render() {
    std::cout << "State::render()" << std::endl;
}

void State::load() {
    std::cout << "State::load()" << std::endl;
}

void State::unload() {
    std::cout << "State::unload()" << std::endl;
}
