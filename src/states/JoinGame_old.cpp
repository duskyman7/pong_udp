//
// Created by Duso on 24. 8. 2016.
//

#include "JoinGame.h"
#include "../main/StateManager.h"
#include "../main/MatchData.h"

void JoinGame::input()
{
    while (m_sm->m_window.pollEvent(e)) {
        input_manual() || input_main();
    }
}

bool JoinGame::input_manual()
{
    if (!show_manual) {
        return false;
    }

    if (e.type == sf::Event::TextEntered) {

        std::string str = text_manual.getString();

        enum unicode_keys{
            backspace = 8,
            enter = 13,
            escape = 27
        };

        switch (e.text.unicode) {
            case backspace:
                std::cout << "backspace" << std::endl;
                if (str.size() > 0) {
                    text_manual.setString(str.substr(0, str.size() - 1));
                }
                break;

            case enter:
            std::cout << "enter" << std::endl;
                manualJoinHost();
                break;

            case escape:
                std::cout << "escape" << std::endl;
                show_manual = false;
                break;

            default:
                text_manual.setString(text_manual.getString() + static_cast<char>(e.text.unicode));

                break;
        }
    }
    return true;
}

bool JoinGame::input_main()
{
    if (e.type != sf::Event::KeyPressed) {
        return false;
    }

    std::cout << "input_main()" << std::endl;

    switch (e.key.code) {
        case sf::Keyboard::Escape:
            m_sm->changeState(MAIN_MENU);
            break;

        case sf::Keyboard::M:
            show_manual = true;
            break;

        default:
            checkNumbers();
    }

    return true;
}

void JoinGame::update(float time)
{

    if (chosen_host != nullptr) {
        float interval = (float) atof(Resources::getGameData(JOIN_GAME_MESSAGE_INTERVAL).c_str());
        if (join_message_timer.getElapsedTime().asSeconds() >= interval) {
            join_message_timer.restart();
            join_retry_counter++;
            retryJoinHost();
        }
    }

    for (Host &h: servers) {
        h.age += time;
    }

    Network::receiveMessages();
    handleNetworkMessages();
    removeOldHosts();
}

void JoinGame::render()
{
    m_sm->m_window.draw(text);

    for (unsigned int i = 0; i < servers.size(); i++) {
        text_host.setPosition(10, 40 + 25 * i);
        text_host.setString(
                std::to_string(i + 1) + ".\t" + servers[i].id + " (" + std::to_string(servers[i].age) + ")");
        m_sm->m_window.draw(text_host);
    }

    if (show_manual) {
        text_manual.setPosition(
                atoi(Resources::getGameData(WINDOW_WIDTH).c_str()) / 2 - text_manual.getGlobalBounds().width / 2,
                atoi(Resources::getGameData(WINDOW_HEIGHT).c_str()) / 2 -
                text_manual.getFont()->getLineSpacing(text_manual.getCharacterSize()) / 2);

        rect.setSize({text_manual.getGlobalBounds().width + 20,
                      text_manual.getFont()->getLineSpacing(text_manual.getCharacterSize())/* + 20*/});
        rect.setPosition(text_manual.getGlobalBounds().left - 10, text_manual.getPosition().y/* - 10*/);
        m_sm->m_window.draw(rect);

        m_sm->m_window.draw(text_manual);
    }

    m_sm->m_window.draw(text_manual_label);
}

void JoinGame::load()
{
    std::cout << "JoinGame::load()" << std::endl;
    Network::receiveMessages();
    Network::flushMessages();
    servers.clear();
    chosen_host = nullptr;
}

void JoinGame::removeOldHosts()
{
    auto it = servers.begin();
    float time_limit = (float) atof(Resources::getGameData(HOST_CONNECTION_TIMEOUT).c_str());

    while (it != servers.end()) {
        if ((*it).age >= time_limit) {
            std::cout << "Removing old host " << (*it).id << std::endl;
            servers.erase(it);
        } else {
            it++;
        }
    }
}

void JoinGame::handleNetworkMessages()
{
    while (Network::pollMessage(msg)) {
        std::cout << "Polling message" << std::endl;
        if (msg.data.size() == 0) {
            std::cerr << "Empty message was polled!" << std::endl;
            continue;
        }

        std::cout << "Message length: " << msg.data.size() << std::endl;
        std::cout << "Message type: " << msg.getMessageType() << std::endl;

        switch (msg.getType()) {
            case SERVER_BROADCAST_INVITE:
                handleInvitation(msg);
                break;

            case SERVER_JOIN_ACCEPT:
                tryGoToLobby(msg);

            default:
                break;
        }
        if (msg.getType() == SERVER_BROADCAST_INVITE) {

        }
    }
}

void JoinGame::checkNumbers()
{
    std::map<sf::Keyboard::Key, unsigned int> numbers = {
            {sf::Keyboard::Num1, 0},
            {sf::Keyboard::Num2, 1},
            {sf::Keyboard::Num3, 2},
            {sf::Keyboard::Num4, 3},
            {sf::Keyboard::Num5, 4},
            {sf::Keyboard::Num6, 5},
            {sf::Keyboard::Num7, 6},
            {sf::Keyboard::Num8, 7},
            {sf::Keyboard::Num9, 8}
    };

    if (numbers.find(e.key.code) != numbers.end()) {

        unsigned int id = numbers[e.key.code];

        if (servers.size() <= id) {
            std::cerr << "Cannot pick host no. " << id + 1 << std::endl;
            return;
        }

        std::cout << "Joining host... " << id + 1 << std::endl;


        tryJoinHost(servers[id]);
    }
}

void JoinGame::tryJoinHost(Host &server)
{

    chosen_host = &server;

    std::string name_player1 = Resources::getGameData(PLAYER1_NAME);
    std::string name_player2 = Resources::getGameData(PLAYER2_NAME);
    std::string id = Resources::getGameData(LOCAL_ID);

    join_message = Message(server.address, server.port);

    Json json;

    json["names"] = {name_player1.substr(0, 32),
                     name_player2.substr(0, 32)};

    join_message.createJoinGameMessage(id, chosen_host->id, json);

    join_retry_counter = 0;
    join_message_timer.restart();
    Network::send(join_message);
}

void JoinGame::handleInvitation(Message &message)
{
    std::cout << "Invitation received!" << std::endl;
    bool is_new = true;
    auto it = servers.begin();

    while (it != servers.end()) {
        if ((*it).id.compare(msg.getSourceId()) == 0) {
            std::cout << "Updating host " << (*it).id << std::endl;
            is_new = false;
            (*it).age = 0;
            break;
        }
        it++;
    }

    if (is_new) {
        std::cout << "Adding new host!" << std::endl;
        servers.push_back(Host(msg.getSourceId(),
                               msg.addr,
                               msg.port));
    }
}

void JoinGame::tryGoToLobby(Message &message)
{
    if (chosen_host == nullptr) {
        std::cout << "Accept message received, but i am not waiting for any..." << std::endl;
        return;
    }

    if (chosen_host->id != message.getSourceId()) {
        std::cout << "Accept message came from different host!" << std::endl;
        return;
    }

    std::cout << "Accept message arrived, going into the lobby" << std::endl;

    MatchData::initFromJoinGame(message);

    m_sm->changeState(LOBBY_CLIENT);
}

void JoinGame::retryJoinHost()
{
    if (join_retry_counter > (unsigned int) atoi(Resources::getGameData(JOIN_GAME_MESSAGE_COUNTER_LIMIT).c_str())) {
        std::cout << "Failed to join to server " << chosen_host->id << std::endl;
        chosen_host = nullptr;
        join_retry_counter = 0;
        return;
    }
    Network::send(join_message);
}

void JoinGame::manualJoinHost()
{
    std::vector<std::string> input = Resources::searchAll(text_manual.getString(), std::regex("\\S+"));

    std::cout << "Input(size: " << input.size() << "): ";

    for (std::string &s : input){
        std::cout << s << " ";
    }

    if (input.size() != 3){
        return;
    }

    std::cout << std::endl;

    if (input[0].size() != (unsigned int) atoi(Resources::getGameData(ID_LENGTH).c_str())){
        std::cerr << "JoinGame::manualJoinHost(): bad ID specified!" << std::endl;
        return;
    }

    if (sf::IpAddress(input[1]) == sf::IpAddress::None){
        std::cerr << "JoinGame::manualJoinHost(): bad IP specified!" << std::endl;
        return;
    }

    if (atoi(input[2].c_str()) >= UINT16_MAX || atoi(input[2].c_str()) < 1){
        std::cerr << "JoinGame::manualJoinHost(): bad port specified!" << std::endl;
        return;
    }

    Host h(input[0], sf::IpAddress(input[1]), (unsigned short) atoi(input[2].c_str()));
    h.names = {"Manual Server", "Manual Server"};
    servers.push_back(h);
    tryJoinHost(servers[servers.size()-1]);
    show_manual = false;
}


