//
// Created by Duso on 24. 8. 2016.
//

#ifndef PONG_UDP_MAINMENU_H
#define PONG_UDP_MAINMENU_H


#include <SFML/Graphics/Text.hpp>
#include "State.h"
#include "../main/Resources.h"

class MainMenu: public State {
public:
    MainMenu(StateManager *sm): State(sm) {
        text.setString("MainMenu");
        text.setCharacterSize(20);
        text.setFillColor(sf::Color::Yellow);
        text.setFont(Resources::getFont(FONT1));
        text.setPosition(atoi(Resources::getGameData(WINDOW_WIDTH).c_str())/2 - text.getGlobalBounds().width/2,
                         10);
    };

    virtual void input() override;

    virtual void update(float time) override;

    virtual void render() override;

private:
    sf::Text text;
    void handleKey(sf::Event event);
};


#endif //PONG_UDP_MAINMENU_H
