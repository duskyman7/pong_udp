//
// Created by duso on 24-Jan-17.
//

#include "GameClient.h"
#include "../main/StateManager.h"
#include "../main/Network.h"
#include "../main/MatchData.h"

GameClient::GameClient(StateManager *sm) : Game(sm)
{
    title.setString("Game");
}

void GameClient::input()
{
    while (m_sm->m_window.pollEvent(e)) {
        if (e.type == sf::Event::MouseMoved) {
            handleMouse();
        }
        if (e.type == sf::Event::KeyPressed) {
            handleKeys();
        }
    }
}

void GameClient::update(float time)
{
    Network::receiveMessages();

    handleMessages();

    local_frame++;

    sendMessage();
}

void GameClient::load()
{
    local_frame = 0;
    MatchData::frame = 0;

    resetBall();
    resetPaddles();
}

void GameClient::handleMouse()
{
    // save the most recent mouse position
    mouse_pos.x = e.mouseMove.x;
    mouse_pos.y = e.mouseMove.y;
}

void GameClient::handleKeys()
{
    if (e.key.code == sf::Keyboard::Escape){
        m_sm->changeState(MAIN_MENU);
    }
}

void GameClient::handleMessages()
{
    while(Network::pollMessage(msg)){
        if (msg.getType() == SERVER_GAME_DATA){
            handleServerData();
        }
    }
}

void GameClient::sendMessage()
{
    Json json;

    typedef MessageJsonFields f;

    json[f::ID] = Resources::getGameData(LOCAL_ID);
    json[f::PADDLE_L] = mouse_pos.y;
    json[f::FRAME] = local_frame;

    msg.addr = MatchData::server.address;
    msg.port = MatchData::server.port;
    msg.data.clear();
    msg.data += (char)CLIENT_GAME_DATA;
    msg.data += json.dump();

    Network::send(msg);
}

void GameClient::handleServerData()
{
    Json json = msg.getJson();
    typedef MessageJsonFields f;

    if (json[f::ID] == MatchData::server.id && (uint32_t)json[f::FRAME] > MatchData::frame){
        MatchData::frame = json[f::FRAME];
        ball.setPosition(json[f::BALL][0],json[f::BALL][1]);

        float x = paddles[0].getPosition().x;
        paddles[0].setPosition(x,json[f::PADDLE_L]);

        x = paddles[1].getPosition().x;
        paddles[1].setPosition(x,json[f::PADDLE_R]);

        std::cout << "left: " << json[f::PADDLE_L] << ", right: " << json[f::PADDLE_R] << ", ball: " << json[f::BALL][0] << " " << json[f::BALL][1] << std::endl;
    }
    else{
        std::cout << "json ID: " << json[f::ID] << "mdata ID: " << MatchData::server.id << std::endl;
        std::cout << "json frame: " << json[f::FRAME] << "mdata frame: " << MatchData::frame << std::endl << std::endl;
    }
}
