#include "LobbyServer.h"
#include "../main/Network.h"
#include "../main/MatchData.h"
#include "../main/StateManager.h"

void LobbyServer::input()
{
    while (m_sm->m_window.pollEvent(e)) {
        if (e.type == sf::Event::KeyPressed){
            switch (e.key.code) {
                case sf::Keyboard::Escape:
                    m_sm->changeState(MAIN_MENU);
                    break;
                case sf::Keyboard::Return:
                    startGame();
                    break;
                default:
                    break;
            }
        }
    }
}

void LobbyServer::update(float time)
{
    MatchData::frame++;
    sendBroadcastMessage();
    handleNetworkMessages();
    updatePlayerAge(time);
    updatePlayerContact(time);
}

void LobbyServer::load()
{
    std::cout << "LobbyServer::load()" << std::endl;
    Network::flushMessages();
    broadcast_timer.restart();
    MatchData::frame = 0;
    MatchData::last_change_frame = 0;
    MatchData::players.clear();

    Player p;
    p.id = Resources::getGameData(LOCAL_ID);
    p.name = Resources::getGameData(PLAYER1_NAME);
    p.color = Resources::getGameData(PLAYER1_COLOR);
    p.age = 0;

    MatchData::players.push_back(p);
    MatchData::server.id = Resources::getGameData(LOCAL_ID);
//    server_id = Resources::getGameData(LOCAL_ID);
}

void LobbyServer::render()
{
    Lobby::render();
}

void LobbyServer::sendBroadcastMessage()
{
    if (broadcast_timer.getElapsedTime().asSeconds() < atof(Resources::getGameData(BROADCAST_INTERVAL).c_str())) {
//        std::cout << broadcast_timer.getElapsedTime().asSeconds() << " < " << atof(Resources::getGameData(BROADCAST_INTERVAL).c_str()) << std::endl;
        return;
    }
    broadcast_timer.restart();
    std::cout << "Sending broadcast" << std::endl;
    Message m(sf::IpAddress::Broadcast, atol(Resources::getGameData(DEFAULT_PORT).c_str()));
    m.createBroadcastMessage(Resources::getGameData(LOCAL_ID), Resources::getGameData(LOCAL_GAME_NAME));
    Network::send(m);
}

void LobbyServer::handleNetworkMessages()
{
    Network::receiveMessages();
    while (Network::pollMessage(msg)) {
        switch (msg.getType()) {
            case CLIENT_REQUEST_JOIN:
                handleJoinRequest();
                break;
            case HOST_KEEPALIVE:
                handleKeepalive();
                break;
            default:
                break;
        }
    }
}

void LobbyServer::handleJoinRequest()
{
    if (MatchData::players.size() >= (unsigned int)(atol(Resources::getGameData(MAX_PLAYERS).c_str()))){
        std::cout << "A player wants to join, but the lobby is full already..." << std::endl;
        return;
    }

    Player player(msg);

    auto it = MatchData::players.begin();

    bool isNew = true;

    while (it != MatchData::players.end()){
        if (player.id == it->id){
            isNew = false;
            it->age = 0;
            sendLobbyData(player);
            break;
        }
        it++;
    }

    if (isNew){
        std::cout << "Adding new player to game!" << std::endl;
        MatchData::players.push_back(player);
        MatchData::aliases[player.id] = std::to_string(alias);
        alias++;
        sendLobbyData(player);
    }
}

void LobbyServer::handleKeepalive()
{
    typedef MessageJsonFields f;
    Json json = msg.getJson();
    std::string id = json[f::ID];
    uint32_t frame = Message::bytesToInt(json[f::FRAME]);

    for (Player &p: MatchData::players){
        if (p.id == id){
            p.age = 0;
            p.frame = frame;
            if (frame < MatchData::last_change_frame){
                sendLobbyData(p);
            }
        }
    }


}

void LobbyServer::sendLobbyData(Player &player)
{
    msg.addr = player.address;
    msg.port = player.port;
    if (player.frame == 0){
        MatchData::last_change_frame = MatchData::frame;
        msg.createServerJoinAcceptMessage(MatchData::players, MatchData::frame, player.id, MatchData::server);
    }
    else{
        msg.createServerLobbyDataMessage(MatchData::players, MatchData::frame, player.id, MatchData::server);
    }
    Network::send(msg);
}

void LobbyServer::updatePlayerAge(float t)
{
    bool change = false;

    auto it = MatchData::players.begin();

    while (it != MatchData::players.end()){
        if (it->id == Resources::getGameData(LOCAL_ID)){
            it->age = 0;
        }
        else{
            it->age += t;
        }

        if (it->age > atof(Resources::getGameData(HOST_CONNECTION_TIMEOUT).c_str())){
            MatchData::players.erase(it);
            change = true;
        }
        else{
            it++;
        }
    }

    if (change){
        for (Player &p: MatchData::players){
            if (p.id != MatchData::server.id){
                sendLobbyData(p);
            }
        }
    }
}

void LobbyServer::updatePlayerContact(float time)
{
    for (Player &p: MatchData::players){
        if (p.id == MatchData::server.id){
            continue;
        }

        p.contact += time;

        if (p.contact > atof(Resources::getGameData(KEEPALIVE_INTERVAL).c_str())){
            sendKeepaliveMessage(p);
            p.contact = 0;
        }
    }
}

void LobbyServer::sendKeepaliveMessage(Player &player)
{
    msg = Message(player.address,player.port);
    msg.createKeepAliveMessage(MatchData::frame);
    Network::send(msg);
}

void LobbyServer::startGame()
{
    if (MatchData::players.size() < 2){
        std::cout << "Can't start, need at least two players!" << std::endl;
        return;
    }

    m_sm->changeState(LOAD_GAME);
}

