//
// Created by duso on 24-Jan-17.
//

#include "GameServer.h"
#include "Game.h"
#include "../main/StateManager.h"

Game::Game(StateManager *sm) : State(sm)
{
    std::cout << "Game::Game()" << std::endl;

    title.setString("Game");
    title.setCharacterSize(20);
    title.setFillColor(sf::Color::Yellow);
    title.setFont(Resources::getFont(FONT1));
    title.setPosition(atoi(Resources::getGameData(WINDOW_WIDTH).c_str()) / 2 - title.getGlobalBounds().width / 2,
                      10);

    ball.setFillColor(sf::Color(255, 255, 0));
    ball.setRadius(10);

    ball_acceleration = 0.5;


    paddles[0].setSize(paddle_size);
    paddles[1].setSize(paddle_size);

    bound_bot = atol(Resources::getGameData(WINDOW_HEIGHT).c_str());
    bound_left = 0;
    bound_right = atol(Resources::getGameData(WINDOW_WIDTH).c_str());
    bound_top = 50;

    panel.setFillColor(sf::Color(64, 64, 64));
    panel.setPosition(0, 0);
    panel.setSize({static_cast<float>(atol(Resources::getGameData(WINDOW_WIDTH).c_str())),
                   50});
}

void Game::render()
{
    m_sm->m_window.draw(title);
    m_sm->m_window.draw(panel);
    m_sm->m_window.draw(paddles[0]);
    m_sm->m_window.draw(paddles[1]);
    m_sm->m_window.draw(ball);
}

void Game::resetBall()
{
    int x = (bound_right - bound_left) / 2;
    int y = (bound_bot - bound_top) / 2;

    ball.setPosition(x, y);

    ball_speed = 2;
    ball_direction = ((float) (rand())) / RAND_MAX * (2 * PI);
    ball_rotation = 0; //0.005;
}

void Game::resetPaddles()
{
    float y = ((bound_bot - bound_top) / 2) - (paddle_size.y / 2);
    paddles[0].setPosition(0, y);
    paddles[1].setPosition(bound_right - paddle_size.x, y);
}
