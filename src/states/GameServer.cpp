//
// Created by duso on 05-Jan-17.
//

#include "GameServer.h"
#include "../main/Network.h"
#include "../main/MatchData.h"
#include "../data/Player.h"
#include "../main/StateManager.h"

GameServer::GameServer(StateManager *sm) : Game(sm)
{

}

void GameServer::input()
{
    while (m_sm->m_window.pollEvent(e)) {
        if (e.type == sf::Event::MouseMoved) {
            handleMouse();
        }
        if (e.type == sf::Event::KeyPressed) {
            handleKeys();
        }
    }
}

void GameServer::update(float time)
{
    receiveMessages();

    updateBall();

    // CPU
    /*{
        float y = ball.getPosition().y - paddle_size.y / 2;

        if (y < bound_top) {
            y = bound_top;
        } else if (y + paddle_size.y > bound_bot) {
            y = bound_bot - paddle_size.y;
        }

        paddles[1].setPosition(bound_right - paddle_size.x, y);
    }*/
    MatchData::frame++;

    sendMessages();
}

void GameServer::render()
{
    m_sm->m_window.draw(title);
    m_sm->m_window.draw(panel);
    m_sm->m_window.draw(paddles[0]);
    m_sm->m_window.draw(paddles[1]);
    m_sm->m_window.draw(ball);
}

void GameServer::load()
{
    MatchData::frame = 0;

    players_frames.clear();
    for (Player &p: MatchData::players) {
        players_frames[p.id] = 0;
    }

    resetBall();
    resetPaddles();
}

void GameServer::handleMouse()
{
//    std::cout << "Mouse Y position: " << e.mouseMove.y << std::endl;

    std::string &my_id = Resources::getGameData(LOCAL_ID);
    tryMovePaddle(my_id, e.mouseMove.y);
}

void GameServer::handleKeys()
{
    if (e.key.code == sf::Keyboard::Escape) {
        exitGame();
    }
}

void GameServer::exitGame()
{
    std::cout << "GameServer::exitGame()" << std::endl;
    m_sm->changeState(MAIN_MENU);
}

void GameServer::receiveMessages()
{
    Network::receiveMessages();

    while (Network::pollMessage(msg)) {
        switch (msg.getType()) {
            case CLIENT_GAME_DATA:
                handleClientData();
            default:
                break;
        }
    }
}

void GameServer::sendMessages()
{
    Json json;

    typedef MessageJsonFields f;
    std::vector<float> position(2);

    json[f::ID] = MatchData::server.id;

    json[f::PADDLE_L] = paddles[0].getPosition().y;

    json[f::PADDLE_R] = paddles[1].getPosition().y;

    position[0] = ball.getPosition().x;
    position[1] = ball.getPosition().y;
    json[f::BALL] = position;

    json[f::FRAME] = MatchData::frame;

    msg.data.clear();

    msg.data += (char) EnumMessageType::SERVER_GAME_DATA;
    msg.data += json.dump();

    for (Player &p: MatchData::players) {

        // skip sending data to server
        if (p.id == MatchData::server.id) {
            continue;
        }
        msg.addr = p.address;
        msg.port = p.port;
        Network::send(msg);
    }
}

void GameServer::tryMovePaddle(std::string id, int y)
{
    int index = -1;

    if (MatchData::players[0].id == id) {
        index = 0;
    } else if (MatchData::players[1].id == id) {
        index = 1;
    }

    if (index == -1) {
        return;
    }

    y = (int) round(y - (paddles[index].getSize().y / 2));
    int x = (int) paddles[index].getPosition().x;

    if (y < bound_top) {
        y = bound_top;
    } else if (y > bound_bot - paddles[index].getSize().y) {
        y = (int) (bound_bot - paddles[index].getSize().y);
    }

    paddles[index].setPosition(x, y);
}

void GameServer::updateBall()
{
    sf::Vector2f new_position = ball.getPosition();

    ball_direction += ball_rotation;

    new_position.x += (float) (cos(ball_direction) * ball_speed);
    new_position.y += (float) (sin(ball_direction) * ball_speed);

    moveBall(new_position);

    float radius = ball.getRadius();

    if (ball.getPosition().x + radius * 2 < bound_left || ball.getPosition().x > bound_right) {
        std::cout << "Ball went out from the game!" << std::endl;
        resetBall();
        resetPaddles();
    }
}

void GameServer::moveBall(sf::Vector2f new_pos)
{
    float radius = ball.getRadius();
    bool bounce = false;

    // collision s lavym paddle
    if (testWithPaddle(new_pos, paddles[0])) {
        float paddle_right = paddles[0].getPosition().x + paddles[0].getSize().x;
        new_pos.x = 2 * (paddle_right) - new_pos.x;
        ball_direction = PI - ball_direction;
        if (ball_direction < 0) {
            ball_direction += 2 * PI;
        }
        bounce = true;
        ball_speed += ball_acceleration;
        // collision s pravym paddle
    } else if (testWithPaddle(new_pos, paddles[1])) {
        float paddle_left = paddles[1].getPosition().x;
        new_pos.x = 2 * (paddle_left - radius * 2) - new_pos.x;
        ball_direction = PI - ball_direction;
        if (ball_direction < 0) {
            ball_direction += 2 * PI;
        }
        bounce = true;
        ball_speed += ball_acceleration;
    } else if (new_pos.y < bound_top) {
        new_pos.y = bound_top + (bound_top - new_pos.y);
        ball_direction = 2 * PI - ball_direction;
        bounce = true;
    } else if (new_pos.y + radius * 2 > bound_bot) {
        ball_direction = 2 * PI - ball_direction;
        new_pos.y = 2 * (bound_bot - radius * 2) - new_pos.y;
        bounce = true;
    }

    /*
    if (new_pos.x - radius < bound_left) {
        pos.x = bound_left + radius;
        pos.y = old_pos.y + ((old_pos.x - pos.x) / (old_pos.x - new_pos.x)) * (new_pos.y - old_pos.y);
        new_pos.x = bound_left + radius + (bound_left + radius - new_pos.x);
        ball_direction = PI - ball_direction;
        if (ball_direction < 0) {
            ball_direction += 2 * PI;
        }
        bounce = true;
    } else if (new_pos.y - radius < bound_top) {
        new_pos.y = bound_top + radius + (bound_top + radius - new_pos.y);
        ball_direction = 2 * PI - ball_direction;
        bounce = true;
    } else if (new_pos.x + radius > bound_right) {
        pos.x = bound_right - radius;
        pos.y = old_pos.y + ((old_pos.x - pos.x) / (old_pos.x - new_pos.x)) * (new_pos.y - old_pos.y);
        new_pos.x = bound_right - radius + (bound_right - radius - new_pos.x);
        ball_direction = PI - ball_direction;
        if (ball_direction < 0) {
            ball_direction += 2 * PI;
        }
    } else if (new_pos.y + radius > bound_bot) {
        ball_direction = 2 * PI - ball_direction;
        new_pos.y = bound_bot - radius + (bound_bot - radius - new_pos.y);
        bounce = true;
    }
     */

    if (bounce) {
        moveBall(new_pos);
    } else {
        ball.setPosition(new_pos);
    }

    /*float radius = ball.getRadius();
    bool bounce = false;

    if (new_pos.x - radius < bound_left){
        bounce = true;
    }
    else if (new_pos.y - radius < bound_top){
        bounce = true;
    }
    else if (new_pos.x + radius < bound_right){
        bounce = true;
    }
    else if (new_pos.y + radius < bound_bot){
        bounce = true;
    }

    if (!bounce){
        return;
    }

    float dist_x = (new_pos.x - old_pos.x);
    float dist_y = (new_pos.y - old_pos.y);

    int *y_axis = nullptr;
    int *x_axis = nullptr;

    int znamienko_x = (dist_x < 0) ? 1 : -1;
    int znamienko_y = (dist_y < 0) ? -1 : 1;

    if (new_pos.x - radius < bound_left){
        y_axis = &bound_left;
    }
    else if (new_pos.x + radius < bound_right){
        y_axis = &bound_right;
    }

    if (new_pos.y - radius < bound_top){
        x_axis = &bound_top;
    }
    else if (new_pos.y + radius < bound_bot){
        x_axis = &bound_bot;
    }

    sf::Vector2f crossPoint_x;
    sf::Vector2f crossPoint_y;

    sf::Vector2f *correctCrossPoint = nullptr;

    if (y_axis != nullptr){
        crossPoint_x.x = *y_axis + (znamienko_x * radius);

        float ratio = (old_pos.x - crossPoint_x.x) / dist_x;

        crossPoint_x.y = old_pos.y + (dist_y * ratio);

        if (crossPoint_x.y + (znamienko_y * radius) < ){

        }
    }*/
}

bool GameServer::testWithPaddle(sf::Vector2f ball_pos, sf::RectangleShape &paddle)
{
    sf::Vector2f paddle_position = paddle.getPosition();
    sf::Vector2f paddle_size = paddle.getSize();

    float radius = ball.getRadius();

    // test vlavo
    if (ball_pos.x < (paddle_position.x + paddle_size.x) && (ball_pos.x + radius > paddle_position.x)) {
        float x = ((ball_pos.x + radius) < paddle_position.x + paddle_size.x) ? 0 : (ball_pos.x + radius) -
                                                                                    (paddle_position.x + paddle_size.x);
        float y = (float) sqrt(fabs((radius * radius) - (x * x)));

        if (ball_pos.y > (paddle_position.y - y) && ball_pos.y < (paddle_position.y + y + paddle_size.y)) {
            return true;
        }
        // test vpravo
    } else if ((ball_pos.x + radius * 2) > paddle_position.x &&
               ((ball_pos.x + radius) < paddle_position.x + paddle_size.x)) {
        float x = (ball_pos.x + radius > paddle_position.x) ? 0 : paddle_position.x - (ball_pos.x + radius);
        float y = (float) sqrt(fabs((radius * radius) - (x * x)));

        if (ball_pos.y > (paddle_position.y - y) && ball_pos.y < (paddle_position.y + y + paddle_size.y)) {
            return true;
        }
    }
    return false;
}

void GameServer::handleClientData()
{
    Json json = msg.getJson();

    typedef MessageJsonFields f;

    std::string id = json[f::ID];
    int y = json[f::PADDLE_L];
    int frame = json[f::FRAME];

    // check if this message is not obsolete
    if (frame > players_frames[id]) {
        tryMovePaddle(id, y);
    }
}
