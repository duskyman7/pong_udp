//
// Created by Duso on 24. 8. 2016.
//

#ifndef PONG_UDP_LOBBYCLIENT_H
#define PONG_UDP_LOBBYCLIENT_H


#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Clock.hpp>
#include "State.h"
#include "../main/Resources.h"
#include "../main/Message.h"
#include "Lobby.h"

class LobbyClient : public Lobby
{

public:
    LobbyClient(StateManager *pManager);

    virtual void input() override;

    virtual void update(float time) override;

    virtual void render() override;

    virtual void load() override;

private:
    sf::Clock timer_keepalive;
    sf::Clock timer_timeout;
    Message keepalive;
    Message msg;

    void sendKeepaliveMessage();

    void handleNetworkMessages();

    void handleLobbyDataMessage();

    void handleKeepalive();
};


#endif //PONG_UDP_LOBBYCLIENT_H
