//
// Created by duso on 24-Dec-16.
//

#ifndef PONG_UDP_LOADGAME_H
#define PONG_UDP_LOADGAME_H


#include <SFML/Graphics/Text.hpp>
#include <SFML/System/Clock.hpp>
#include "State.h"
#include "../main/Message.h"

class LoadGame: public State
{
public:
    LoadGame(StateManager *sm);

    virtual void render() override;

    virtual void input() override;

    virtual void update(float time) override;

    virtual void load() override;

private:
    sf::Text title;
    sf::Text text;
    sf::Clock timer;


    int load_max;
    int load_current;
    int frame;

    std::map<std::string,int> players_load;

    void interrupt_loading();

    void sendLoadInfo();

    Message msg;

    void handleNetwork();

    void HandleServerGameLoading();

    void HandleClientGameLoading();

    void checkAllReady();

    void goToGame();

    void tryGoToGame();
};


#endif //PONG_UDP_LOADGAME_H
