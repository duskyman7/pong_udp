//
// Created by Duso on 24. 8. 2016.
//

#include <iostream>
#include "MainMenu.h"
#include "../main/StateManager.h"



void MainMenu::input()
{
//    std::cout << "MainMenu::input()" << std::endl;
    while(m_sm->m_window.pollEvent(e)){
        switch (e.type){
            case sf::Event::KeyPressed:
                handleKey(e);
                break;

            case sf::Event::Closed:
                m_sm->stop();
                break;
            default:
                break;
        }
    }
}

void MainMenu::update(float time)
{
//    std::cout << "MainMenu::update()" << std::endl;
}

void MainMenu::render()
{
    m_sm->m_window.draw(text);
}

void MainMenu::handleKey(sf::Event event)
{
    if (e.type == sf::Event::KeyPressed){
        switch (event.key.code){
            case sf::Keyboard::Num1:
                m_sm->changeState(LOBBY_SERVER);
                break;
            case sf::Keyboard::Num2:
                m_sm->changeState(JOIN_GAME);
                break;
            case sf::Keyboard::Escape:
                m_sm->stop();
            default:
                break;
        }
    }
}


