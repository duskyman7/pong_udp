//
// Created by Duso on 24. 8. 2016.
//

#ifndef PONG_UDP_LOBBYSERVER_H
#define PONG_UDP_LOBBYSERVER_H


#include <SFML/System/Clock.hpp>
#include <SFML/Graphics/Text.hpp>
#include "State.h"
#include "../main/Resources.h"
#include "../main/Message.h"
#include "Lobby.h"
#include "../main/Host.h"
#include "../main/MatchData.h"

class LobbyServer: public Lobby {

public:
    LobbyServer(StateManager *pManager) : Lobby(pManager) {
        std::cout << "LobbyServer::LobbyServer()" << std::endl;

        MatchData::frame = 1;

        title.setString("LobbyServer");
        title.setCharacterSize(20);
        title.setFillColor(sf::Color::Yellow);
        title.setFont(Resources::getFont(FONT1));
        title.setPosition(atoi(Resources::getGameData(WINDOW_WIDTH).c_str())/2 - title.getGlobalBounds().width/2, 10);
    }

    virtual void input() override;

    virtual void update(float time) override;

    virtual void render() override;

    virtual void load() override;

private:
    std::vector<bool> clients_ready;
    bool game_starting = false;

    void update_lobby(float time);
    void update_game_starting(float time);
    void handleNetworkMessages();
    void removeOldClients();
    void refreshClientAge(Message &message);
    void send_full_lobby_data_message(Host &client);
    void send_broadcast_message();
    void handleJoinRequest(Message &message);
    sf::Clock broadcast_timer;

    void switch_to_game_starting();

    void send_game_starting_message();
};


#endif //PONG_UDP_LOBBYSERVER_H
