//
// Created by Duso on 24. 8. 2016.
//

#ifndef PONG_UDP_LOBBYSERVER_H
#define PONG_UDP_LOBBYSERVER_H


#include <SFML/System/Clock.hpp>
#include "Lobby.h"

#include "../data/Player.h"

class LobbyServer : public Lobby
{

public:
    LobbyServer(StateManager *pManager) : Lobby(pManager)
    {
        std::cout << "LobbyServer::LobbyServer()" << std::endl;
        title.setString("LobbyServer");
        title.setCharacterSize(20);
        title.setFillColor(sf::Color::Yellow);
        title.setFont(Resources::getFont(FONT1));
        title.setPosition(atoi(Resources::getGameData(WINDOW_WIDTH).c_str()) / 2 - title.getGlobalBounds().width / 2,
                          10);
    }

    virtual void input() override;

    virtual void update(float time) override;

    virtual void load() override;

    virtual void render() override;

private:
    sf::Clock broadcast_timer;
    Message msg;
    int alias=0;

    void sendBroadcastMessage();

    void handleNetworkMessages();

    void handleJoinRequest();


    void handleKeepalive();

    void sendLobbyData(Player &player);

    void updatePlayerAge(float t);

    void updatePlayerContact(float time);

    void sendKeepaliveMessage(Player &player);

    void startGame();
};


#endif //PONG_UDP_LOBBYSERVER_H
