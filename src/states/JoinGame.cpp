#include <fstream>
#include "JoinGame.h"
#include "../main/Network.h"
#include "../main/MatchData.h"

void JoinGame::input()
{
    while (m_sm->m_window.pollEvent(e)) {
        input_main();
    }
}

void JoinGame::update(float time)
{
    handleNetworkMessages();
    updateServersAge(time);
    resendJoinMessage();
}

void JoinGame::render()
{
    m_sm->m_window.draw(text);

    for (unsigned int i = 0; i < servers.size(); i++) {
        text_host.setPosition(10, 40 + 25 * i);
        text_host.setString(
                std::to_string(i + 1) + ". " + servers[i].name + " (" + std::to_string(servers[i].age) + ")");
        m_sm->m_window.draw(text_host);
    }
}

void JoinGame::load()
{
    servers.clear();
    Network::flushMessages();
    chosen_host = nullptr;
    join_retry_counter = 0;
}

void JoinGame::handleBroadcast()
{
    std::cout << "JoinGame::handleBroadcast()" << std::endl;
    Server server(msg);

    std::cout << "Invitation received!" << std::endl;
    bool is_new = true;
    auto it = servers.begin();

    while (it != servers.end()) {
        if (it->id == server.id) {
            std::cout << "Updating game (" << it->name << ")" << std::endl;
            is_new = false;
            it->age = 0;
            break;
        }
        it++;
    }

    if (is_new) {
        std::cout << "Adding new game! (" << server.name << ")" << std::endl;
        servers.push_back(server);
    }
}

void JoinGame::handleNetworkMessages()
{
    Network::receiveMessages();

    while (Network::pollMessage(msg)) {
        switch (msg.getType()) {

//            case BAD_MESSAGE:break;
            case SERVER_BROADCAST_INVITE:
                std::cout << "Received broadcast message" << std::endl;
                handleBroadcast();
                break;
//            case CLIENT_REQUEST_JOIN:break;
//            case HOST_KEEPALIVE:break;
            case SERVER_JOIN_ACCEPT:
                handleJoinAccept();
                break;
//            case SERVER_LOBBY_DATA:break;
//            case SERVER_GAME_LOADING:break;
            default:
                break;
        }
    }
}

void JoinGame::updateServersAge(float t)
{
    auto it = servers.begin();

    while (it != servers.end()) {
        it->age += t;

        if (it->age > atof(Resources::getGameData(HOST_CONNECTION_TIMEOUT).c_str())) {
            servers.erase(it);
        } else {
            it++;
        }
    }
}

void JoinGame::input_main()
{
    if (e.type == sf::Event::KeyPressed){
        switch (e.key.code) {
            case sf::Keyboard::Escape:
                m_sm->changeState(MAIN_MENU);
                break;

            case sf::Keyboard::M:
                manual_connect();
                break;
            default:
                checkNumbers();
                break;
        }
    }
}

void JoinGame::checkNumbers()
{
    std::map<sf::Keyboard::Key, unsigned int> numbers = {
            {sf::Keyboard::Num1, 0},
            {sf::Keyboard::Num2, 1},
            {sf::Keyboard::Num3, 2},
            {sf::Keyboard::Num4, 3},
            {sf::Keyboard::Num5, 4},
            {sf::Keyboard::Num6, 5},
            {sf::Keyboard::Num7, 6},
            {sf::Keyboard::Num8, 7},
            {sf::Keyboard::Num9, 8}
    };

    if (numbers.find(e.key.code) == numbers.end()) {
        return;
    }

    unsigned int id = numbers[e.key.code];

    if (servers.size() <= id) {
        std::cout << "Cannot pick host no. " << id + 1 << std::endl;
        return;
    }

    std::cout << "Joining host... " << id + 1 << std::endl;

    tryJoinServer(servers[id]);
}

void JoinGame::tryJoinServer(Server &server)
{
    chosen_host = &server;
    join_retry_counter = 0;
    join_message_timer.restart();
    join_message = Message(server.address, server.port);
    join_message.createJoinGameMessage();
    Network::send(join_message);
}

void JoinGame::handleJoinAccept()
{
    if (chosen_host == nullptr) {
        std::cout << "Accept message received, but i am not waiting for any..." << std::endl;
        return;
    }

    Json json = msg.getJson();

    typedef MessageJsonFields f;

    if (chosen_host->id != json[f::ID]) {
        std::cout << "Accept message came from different host!" << std::endl;
        return;
    }

    std::cout << "Accept message arrived, going into the lobby" << std::endl;

    MatchData::initFromJoinGame(json, chosen_host);

    m_sm->changeState(LOBBY_CLIENT);
}

void JoinGame::resendJoinMessage()
{
    if (chosen_host == nullptr) {
        return;
    }
    if (join_message_timer.getElapsedTime().asSeconds() >
        atof(Resources::getGameData(JOIN_GAME_MESSAGE_INTERVAL).c_str())) {
        join_message_timer.restart();
        if (++join_retry_counter >= atof(Resources::getGameData(JOIN_GAME_MESSAGE_COUNTER_LIMIT).c_str())) {
            std::cerr << "Could not join to game " << chosen_host->name << std::endl;
            chosen_host = nullptr;
            join_retry_counter = 0;
        } else {
            Network::send(join_message);
        }
    }
}

void JoinGame::manual_connect()
{
    std::ifstream file("server.cfg", std::ios::in);

    if (!file.is_open()) {
        std::cerr << "Failed to open server file! Cannot connect manually!" << std::endl;
        return;
    }

    std::string line;

    if (!std::getline(file,line)){
        std::cerr << "server.cfg file is BAD! Cannot connect manually!" << std::endl;
        return;
    }

    sf::IpAddress address(line);

    if (!std::getline(file,line)){
        std::cerr << "server.cfg file is BAD! Cannot connect manually!" << std::endl;
        return;
    }

    unsigned short port = atol(line.c_str());

    if (!std::getline(file,line)){
        std::cerr << "server.cfg file is BAD! Cannot connect manually!" << std::endl;
        return;
    }

    std::cout << "Manual connection to:" << std::endl;
    std::cout << line << " at " << address.toString() << ":" << port << std::endl;

    manual_server = Server(address,port,line,"Manual Connection");

    tryJoinServer(manual_server);
}
