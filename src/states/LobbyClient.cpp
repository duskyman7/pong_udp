//
// Created by Duso on 24. 8. 2016.
//

#include "LobbyClient.h"
#include "../main/StateManager.h"
#include "../main/MatchData.h"
#include "../main/Network.h"

LobbyClient::LobbyClient(StateManager *pManager) : Lobby(pManager)
{
    std::cout << "LobbyClient::LobbyClient()" << std::endl;

    title.setString("LobbyClient");
    title.setPosition(atoi(Resources::getGameData(WINDOW_WIDTH).c_str()) / 2 - title.getGlobalBounds().width / 2,
                      10);
    MatchData::frame = 0;
}

void LobbyClient::input()
{
    while (m_sm->m_window.pollEvent(e)) {
        if (e.type == sf::Event::KeyPressed){
            switch (e.key.code) {
                case sf::Keyboard::Escape:
                    m_sm->changeState(MAIN_MENU);
                    break;
                default:
                    break;
            }
        }
    }
}

void LobbyClient::update(float time)
{
    if (timer_keepalive.getElapsedTime().asSeconds() >=
        atof(Resources::getGameData(KEEPALIVE_INTERVAL).c_str())) {
        sendKeepaliveMessage();
    }

    handleNetworkMessages();
}

void LobbyClient::render()
{
    Lobby::render();
}

void LobbyClient::load()
{
    std::cout << "LobbyClient::load()" << std::endl;
    sendKeepaliveMessage();

    /*std::cout << "Players in the lobby:" << std::endl;

    for (Player &p : MatchData::players){
        std::cout << "    name: " << p.name << ", id: " << p.id << ", is_server: " << p.is_server << std::endl;
    }*/
}

void LobbyClient::sendKeepaliveMessage()
{
    std::cout << "LobbyClient::sendKeepaliveMessage()" << std::endl;
    timer_keepalive.restart();
    keepalive.createKeepAliveMessage(MatchData::frame);
    keepalive.addr = MatchData::server.address;
    keepalive.port = MatchData::server.port;
    Network::send(keepalive);
}

void LobbyClient::handleNetworkMessages()
{
    Network::receiveMessages();

    while (Network::pollMessage(msg)) {
        switch (msg.getType()) {
            case SERVER_GAME_LOADING:
                std::cout << "Game is STARTING!!!" << std::endl;
                m_sm->changeState(LOAD_GAME);
                break;
            case SERVER_LOBBY_DATA:
                handleLobbyDataMessage();
                break;
            case HOST_KEEPALIVE:
                handleKeepalive();
                break;
            default:
                std::cout << "A message arrived, but i do not care about it" << std::endl;
                std::cout << "content: " << msg.data << std::endl;
            std::cout << "type: " << Message::getMessageType(msg.getType()) << std::endl;
                break;
        }
    }
}

void LobbyClient::handleLobbyDataMessage()
{
    std::cout << "LobbyClient::handleLobbyDataMessage()" << std::endl;
    Json json = msg.getJson();

    typedef MessageJsonFields f;

    MatchData::players.clear();

    for (Json player: json[f::PLAYERS]) {
        Player p;
        p.name = player[f::PLAYER_NAME];
        p.color = player[f::COLOR];
        p.id = player[f::ID];

        if (player.find(f::IS_SERVER) != player.end()){
            p.is_server = player[f::IS_SERVER];
        }

        MatchData::players.push_back(p);
    }
}

void LobbyClient::handleKeepalive()
{
    if (msg.getJson()[MessageJsonFields::ID] == MatchData::server.id){
        MatchData::server.age = 0;
    }
}


