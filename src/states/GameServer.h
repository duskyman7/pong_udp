//
// Created by duso on 05-Jan-17.
//

#ifndef PONG_UDP_GAMESERVER_H
#define PONG_UDP_GAMESERVER_H

#include "Game.h"

class GameServer: public Game
{
public:
    GameServer(StateManager *sm);

    virtual void input() override;

    virtual void update(float time) override;

    virtual void render() override;

    virtual void load() override;

private:
    std::map<std::string, int> players_frames;

    void handleMouse();

    void handleKeys();

    void exitGame();

    void receiveMessages();

    void sendMessages();

    void tryMovePaddle(std::string id, int y);

    void updateBall();

    void moveBall(sf::Vector2f new_pos);

    bool testWithPaddle(sf::Vector2f ball_pos, sf::RectangleShape &paddle);

    void handleClientData();
};


#endif //PONG_UDP_GAMESERVER_H
