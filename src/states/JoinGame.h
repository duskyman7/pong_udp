//
// Created by Duso on 24. 8. 2016.
//

#ifndef PONG_UDP_JOINGAME_H
#define PONG_UDP_JOINGAME_H


#include <SFML/Graphics/Text.hpp>
#include "State.h"
#include "../main/StateManager.h"
#include "../main/Resources.h"
#include "../main/Message.h"
#include "../data/Server.h"

class JoinGame : public State
{
public:
    JoinGame(StateManager *sm) : State(sm)
    {
        text_host.setFont(Resources::getFont(FONT1));
        text_host.setFillColor(sf::Color::Yellow);
        text_host.setCharacterSize(20);

        text.setString("JoinGame");
        text.setCharacterSize(20);
        text.setFillColor(sf::Color::Yellow);
        text.setFont(Resources::getFont(FONT1));
        text.setPosition(atoi(Resources::getGameData(WINDOW_WIDTH).c_str()) / 2 - text.getGlobalBounds().width / 2,
                         10);
    }

    virtual void input() override;

    virtual void update(float time) override;

    virtual void render() override;

    virtual void load() override;

private:

    Message msg;

    sf::Text text;
    sf::Text text_host;

    std::vector<Server> servers;

    Server *chosen_host;
    Server manual_server;

    Message join_message;

    void handleBroadcast();

    void handleNetworkMessages();

    void updateServersAge(float t);

    void input_main();

    void checkNumbers();

    void tryJoinServer(Server &server);


    int join_retry_counter = 0;
    sf::Clock join_message_timer;

    void handleJoinAccept();

    void resendJoinMessage();

    void manual_connect();
};


#endif //PONG_UDP_JOINGAME_H
