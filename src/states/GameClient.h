//
// Created by duso on 24-Jan-17.
//

#ifndef PONG_UDP_GAMECLIENT_H
#define PONG_UDP_GAMECLIENT_H


#include "Game.h"

class GameClient: public Game
{
public:
    GameClient(StateManager *sm);

    void input() override;

    void update(float time) override;

    void load() override;

    void handleMouse();

    void handleKeys();

private:
    sf::Vector2i mouse_pos;
    int local_frame;

    void handleMessages();

    void sendMessage();

    void handleServerData();
};


#endif //PONG_UDP_GAMECLIENT_H
