//
// Created by Duso on 23. 8. 2016.
//

#ifndef PONG_UDP_STATE_H
#define PONG_UDP_STATE_H

#include "SFML/Window/Event.hpp"

class StateManager;

class State {
public:
    State(StateManager* sm): m_sm(sm) {}
    virtual void input();
    virtual void update(float time);
    virtual void render();
    virtual void load();
    virtual void unload();

protected:
    StateManager *m_sm;
    sf::Event e;
};


#endif //PONG_UDP_STATE_H
