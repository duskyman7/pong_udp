//
// Created by duso on 24-Jan-17.
//

#ifndef PONG_UDP_GAME_H
#define PONG_UDP_GAME_H

#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/CircleShape.hpp>
#include "State.h"
#include "../main/Resources.h"
#include "../main/Message.h"

class Game: public State
{

public:
    Game(StateManager *sm);

    void render() override;

protected:
    // GUI
    sf::Text title;

    // game elements
    sf::RectangleShape panel;
    sf::RectangleShape paddles[2];
    sf::CircleShape ball;

    int bound_top;
    int bound_bot;
    int bound_left;
    int bound_right;

    float ball_direction;
    float ball_rotation;
    float ball_speed;
    float ball_acceleration;

    sf::Vector2f paddle_size = {10, 50};

    const float PI = 3.1415927f;

    Message msg;

    void resetBall();

    void resetPaddles();
};


#endif //PONG_UDP_GAME_H
