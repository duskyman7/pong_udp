//
// Created by Duso on 10. 9. 2016.
//

#include "Lobby.h"
#include "../main/StateManager.h"
#include "../main/MatchData.h"

void Lobby::render()
{
    m_sm->m_window.draw(title);

    std::string s;

    for (unsigned int i = 0; i < MatchData::players.size(); i++) {

        if (MatchData::players[i].id == Resources::getGameData(LOCAL_ID)) {
            s = " (you)";
        } else if (MatchData::players[i].is_server) {
            s = " (server)";
        } else {
            s = "";
        }

        text_client.setFillColor(Resources::hexToColor(MatchData::players[i].color));
        text_client.setPosition(10, i * 30 + 40);
        text_client.setString(std::to_string(i + 1) + ".   " + MatchData::players[i].name + s);
        m_sm->m_window.draw(text_client);
    }
}
