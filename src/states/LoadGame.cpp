//
// Created by duso on 24-Dec-16.
//

#include <iostream>
#include "LoadGame.h"
#include "../main/Resources.h"
#include "../main/StateManager.h"
#include "../main/MatchData.h"
#include "../data/Player.h"
#include "../main/Network.h"

LoadGame::LoadGame(StateManager *sm) : State(sm)
{
    std::cout << "LoadGame::LoadGame()" << std::endl;

    text.setFont(Resources::getFont(FONT1));
    text.setFillColor(sf::Color::Yellow);
    text.setCharacterSize(20);

    title.setString("LoadGame");
    title.setCharacterSize(20);
    title.setFillColor(sf::Color::Yellow);
    title.setFont(Resources::getFont(FONT1));
    title.setPosition(atoi(Resources::getGameData(WINDOW_WIDTH).c_str()) / 2 - title.getGlobalBounds().width / 2,
                      10);
}

void LoadGame::input()
{
    while (m_sm->m_window.pollEvent(e)) {
        if (e.type == sf::Event::KeyPressed) {
            if (e.key.code == sf::Keyboard::C && sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) {
                interrupt_loading();
            }
        }
    }
}

void LoadGame::update(float time)
{
    if (Resources::getGameData(LOCAL_ID) == MatchData::server.id){
        frame++;
        checkAllReady();
    }

    if (load_current < load_max) {

        load_current += (rand() % 4) * 3;

        if (load_current > load_max){
            load_current = load_max;
        }
    }

    players_load[Resources::getGameData(LOCAL_ID)] = (int) floor(((float) load_current / load_max) * 100);

    handleNetwork();

    sendLoadInfo();
}

void LoadGame::render()
{
    m_sm->m_window.draw(title);

    auto &p = MatchData::players;

    for (unsigned int i = 0; i < p.size(); i++) {
        text.setFillColor(sf::Color::Yellow);
        text.setString(std::to_string(players_load[p[i].id]) + '%');
        text.setPosition(10, i * 30 + 40);
        m_sm->m_window.draw(text);

        text.setFillColor(Resources::hexToColor(p[i].color));
        text.setString(p[i].name);
        text.setPosition(70, i * 30 + 40);
        m_sm->m_window.draw(text);
    }
}

void LoadGame::load()
{
    frame = 0;
    timer.restart();

    // fejkovanie loadovania
    srand(time(NULL));
    load_max = 1800 + ((rand() % 11) * 100);
    load_current = 0;

    players_load.clear();
    for (Player &p: MatchData::players) {
        players_load[p.id] = 0;
    }

    sendLoadInfo();
}

void LoadGame::interrupt_loading()
{
    std::cout << "LoadGame::interrupt_loading()" << std::endl;
    m_sm->changeState(MAIN_MENU);
}

void LoadGame::sendLoadInfo()
{
    if (timer.getElapsedTime().asSeconds() < atof(Resources::getGameData(LOAD_MESSAGE_INTERVAL).c_str())){
        return;
    }

    timer.restart();

    // server
    if (MatchData::server.id == Resources::getGameData(LOCAL_ID)) {

        typedef MessageJsonFields f;

        // send message to each player
        for (Player &dst : MatchData::players) {

            Json json;

            json[f::PLAYERS] = Json::array();
            json[f::ID] = MatchData::server.id;
            json[f::FRAME] = frame;

            for (Player &p: MatchData::players) {
                Json pp;

                // send player's own id or server id, otherwise send alias
                pp[f::ID] = ((p.id == dst.id) || (p.id == MatchData::server.id)) ? p.id : MatchData::aliases[p.id];

                pp[f::PLAYER_NAME] = p.name;
                pp[f::IS_SERVER] = (p.id == MatchData::server.id);
                pp[f::COLOR] = p.color;
                pp[f::LOAD] = players_load[p.id];

                json[f::PLAYERS].push_back(pp);
            }

            msg = Message(dst.address, dst.port);
            msg.createLoadingMessage(json, true);
            Network::send(msg);
        }
    } else { // client
        typedef MessageJsonFields f;

        Json json;

        json[f::ID] = Resources::getGameData(LOCAL_ID);
        json[f::LOAD] = players_load[Resources::getGameData(LOCAL_ID)];
        json[f::FRAME] = frame;

        msg = Message(MatchData::server.address, MatchData::server.port);
        msg.createLoadingMessage(json, false);
        Network::send(msg);
    }
}

void LoadGame::handleNetwork()
{
    Network::receiveMessages();

    while (Network::pollMessage(msg)) {
        switch (msg.getType()){
            case CLIENT_GAME_LOADING:
                HandleClientGameLoading();
                break;

            case SERVER_GAME_LOADING:
                HandleServerGameLoading();
                break;

            case SERVER_GAME_DATA:
                tryGoToGame();
            default:
                break;
        }
    }
}

void LoadGame::HandleServerGameLoading()
{
    // ignore this packet if i am server
    if (Resources::getGameData(LOCAL_ID) == MatchData::server.id){
        return;
    }

    std::cout << "Received ServerGameLoading packet!" << std::endl;

    typedef MessageJsonFields f;
    Json json = msg.getJson();

    if ((int)json[f::FRAME] <= frame){
        std::cout << "the packet is not newer tho..." << std::endl;
        return;
    }

    frame = json[f::FRAME];

    Json players = json[f::PLAYERS];
    for (Json player: players){
        if (player[f::ID] != Resources::getGameData(LOCAL_ID)){
            players_load[player[f::ID]] = player[f::LOAD];
        }
    }
}

void LoadGame::HandleClientGameLoading()
{
    // ignore this packet if i am not server
    if (Resources::getGameData(LOCAL_ID) != MatchData::server.id){
        return;
    }

    std::cout << "Received ClientGameLoading packet!" << std::endl;

    typedef MessageJsonFields f;
    Json json = msg.getJson();
    players_load[json[f::ID]] = json[f::LOAD];
}

void LoadGame::checkAllReady()
{
    bool all_ready = true;

    for (auto &p: players_load){
        if (p.second != 100){
            all_ready = false;
            break;
        }
    }

    if (all_ready){
        goToGame();
    }
}

void LoadGame::goToGame()
{
    std::cout << "Everyone ready, going to game!!!" << std::endl;
    bool isServer = Resources::getGameData(LOCAL_ID) == MatchData::server.id;
    m_sm->changeState(isServer ? GAME_SERVER : GAME_CLIENT);
}

void LoadGame::tryGoToGame()
{
    typedef MessageJsonFields f;

    if (msg.getJson()[f::ID] == MatchData::server.id){
        goToGame();
    }
}
