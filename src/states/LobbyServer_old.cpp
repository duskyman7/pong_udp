//
// Created by Duso on 24. 8. 2016.
//

#include <iostream>
#include "LobbyServer.h"
#include "../main/StateManager.h"
#include "../main/Network.h"
#include "../main/MatchData.h"

using Json = nlohmann::json;

void LobbyServer::input()
{
    while (m_sm->m_window.pollEvent(e)) {
        switch (e.key.code) {
            case sf::Keyboard::Escape:
                m_sm->changeState(MAIN_MENU);
                break;
            case sf::Keyboard::Return:
                if (!game_starting) {
                    switch_to_game_starting();
                }
                break;
            default:
                break;
        }
    }
}

void LobbyServer::update(float time)
{
    if (game_starting) {
        update_game_starting(time);
    } else {
        update_lobby(time);
    }
}

void LobbyServer::render()
{
    Lobby::render();
}

void LobbyServer::load()
{
    std::cout << "LobbyServer::load()" << std::endl;

    game_starting = false;
    MatchData::connected_players.clear();
    clients_ready.clear();

    Host host(Resources::getGameData(LOCAL_ID), sf::IpAddress::getLocalAddress(),
              (unsigned short) atoi(Resources::getGameData(LOCAL_PORT).c_str()));
    host.names = {Resources::getGameData(PLAYER1_NAME), Resources::getGameData(PLAYER2_NAME)};
    MatchData::connected_players.push_back(host);
}

// -----------------------------------------------------------------------------


void LobbyServer::switch_to_game_starting()
{
    game_starting = true;
    broadcast_timer.restart();
    clients_ready.clear();
    clients_ready.resize(MatchData::connected_players.size());
    auto it = clients_ready.begin();
    while (it != clients_ready.end()){
        *it = false;
        it++;
    }
}

void LobbyServer::update_lobby(float time)
{
    MatchData::frame++;

    if (broadcast_timer.getElapsedTime().asSeconds() >= atof(Resources::getGameData(BROADCAST_INTERVAL).c_str())) {
        broadcast_timer.restart();
        send_broadcast_message();
    }

    for (Host &h: MatchData::connected_players) {
        if (h.id == Resources::getGameData(LOCAL_ID)) {
            h.age = 0;
        } else {
            h.age += time;
        }
    }

    handleNetworkMessages();

    removeOldClients();
}

void LobbyServer::update_game_starting(float time)
{
    int count = 0;
    auto it = clients_ready.begin();
    while (it != clients_ready.end()){
        count += *it ? 1 : 0;
        it++;
    }

    std::cout << "Number of ready players: " << count << " / " << clients_ready.size() << std::endl;

    if (broadcast_timer.getElapsedTime().asSeconds() >= atof(Resources::getGameData(BROADCAST_INTERVAL).c_str())) {
        broadcast_timer.restart();
        send_game_starting_message();
    }
}

void LobbyServer::handleJoinRequest(Message &message)
{
    Host client(message);

    bool isNew = true;
    auto it = MatchData::connected_players.begin();

    while (it != MatchData::connected_players.end()) {
        if (it->id == client.id) {
            std::cout << "Client with same ID tried to join(" << client.id << ")... Sending him full data" << std::endl;
            isNew = false;
            send_full_lobby_data_message(client);
            break;
        }
        it++;
    }

    if (isNew) {
        std::cout << "New client has joined!: " << client.id << std::endl;

        std::cout << "Client names: " << std::endl;

        Json names = message.getJson()["names"];

        std::cout << names << std::endl;

        for (std::string s : names) {
            client.names.push_back(s);
            std::cout << s << "";
        }
        std::cout << std::endl;

        MatchData::connected_players.push_back(client);

        send_full_lobby_data_message(client);
    }
}

void LobbyServer::handleNetworkMessages()
{
    Network::receiveMessages();
    Message m;

    while (Network::pollMessage(m)) {
        switch (m.getType()) {
            case CLIENT_REQUEST_JOIN:
                handleJoinRequest(m);
                break;
            case HOST_KEEPALIVE:
                refreshClientAge(m);
            case HOST_GAME_CREATE:

            default:
                break;
        }
    }
}

void LobbyServer::removeOldClients()
{
    auto it = MatchData::connected_players.begin();

    while (it != MatchData::connected_players.end()) {
        if (it->age > atof(Resources::getGameData(MAX_CLIENT_AGE).c_str())) {
            // keby nahodou sa nejako sekol, nech nevymaze sam seba
            if (it->id != Resources::getGameData(LOCAL_ID)) {
                std::cout << "removing client " << (*it).id << std::endl;
                MatchData::connected_players.erase(it);
            }
        } else {
            it++;
        }
    }
}

void LobbyServer::refreshClientAge(Message &message)
{
    std::cout << "Keepalive message from " << message.getSourceId() << std::endl;

    for (Host &h: MatchData::connected_players) {
        if (h.id == message.getSourceId()) {
            h.age = 0;
        }
    }
}

void LobbyServer::send_broadcast_message()
{
    std::cout << "Broadcasting..." << std::endl;

    unsigned short port = (unsigned short) atoi(Resources::getGameData(DEFAULT_PORT).c_str());

    Message notification(sf::IpAddress::Broadcast, port);

    notification.createBroadcastMessage(Resources::getGameData(LOCAL_ID));

    Network::send(notification);
}

void LobbyServer::send_full_lobby_data_message(Host &client)
{
    Json json;

    json["players"] = Json::array();

    {
        Json player;

        for (Host &h: MatchData::connected_players) {
            player["names"] = h.names;
            if (h.id == client.id || h.id == Resources::getGameData(LOCAL_ID)) {
                player["id"] = h.id;
            }
            json["players"].push_back(player);
        }
    }

    std::cout << "Sending full lobby data to id " << client.id << std::endl << json.dump(2) << std::endl;

    Message message(client.address, client.port);

    message.createServerJoinAcceptMessage(client.id, MatchData::frame, json);

//    std::cout << "whole message: " << std::endl << "-----------------------------" << std::endl << message.data << std::endl << "-------------------------" << std::endl;

    Network::send(message);
}

void LobbyServer::send_game_starting_message()
{
    std::cout << "Broadcasting..." << std::endl;

    auto &cp = MatchData::connected_players;

    for (int i=0; i<cp.size(); i++){
        if (clients_ready[i]){
            continue;
        }
        Message notification(cp[i].address, cp[i].port);
        notification.createGameStartMessage(Resources::getGameData(LOCAL_ID), cp[i].id);
        Network::send(notification);
    }
}
