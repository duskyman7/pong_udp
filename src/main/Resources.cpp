//
// Created by Duso on 16. 8. 2016.
//

#include <iostream>
#include <fstream>
#include "Resources.h"

std::map<EnumFonts, sf::Font>       Resources::fonts;
std::map<EnumGameData, std::string> Resources::game_data;

std::vector<std::string> Resources::split(std::string s)
{
    std::regex r("\\S+");
    std::sregex_iterator it(s.begin(), s.end(), r);
    std::sregex_iterator end;

    std::vector<std::string> vec;

    int length = 0;

    while (it != end) {
        length++;
        it++;
    }

    // pokial tu nie su aspon dve slova, vratim prazdny vektor
    if (length < 2) {
        return vec;
    }

    it = std::sregex_iterator(s.begin(), s.end(), r);

    vec.push_back(it->str());
    it++;
    vec.push_back(s.substr(it->position()));

    // inac vratim prve slovo + zvysok
    return vec;
}

void Resources::init()
{
    std::cout << "Resources::init()" << std::endl;

    sf::Font font;
    if (!font.loadFromFile("impact.ttf")) {
        std::cerr << "FAILED TO LOAD IMPACT.TTF!" << std::endl;
    }
    fonts[FONT1] = font;

    // default hardcoded settings
    game_data[BROADCAST_INTERVAL] = "3.0";
    game_data[NETWORK_BUFFER_SIZE] = "1024";
    game_data[DEFAULT_PORT] = "54321";
    game_data[ID_LENGTH] = "16";
    game_data[HOST_CONNECTION_TIMEOUT] = "5";
    game_data[WINDOW_WIDTH] = "640";
    game_data[WINDOW_HEIGHT] = "480";
    game_data[PLAYER1_NAME] = "PLAYER1";
    game_data[PLAYER2_NAME] = "PLAYER2";
    game_data[MAX_PLAYERS] = "10";
    game_data[MAX_CLIENT_AGE] = "3";
    game_data[JOIN_GAME_MESSAGE_INTERVAL] = "1";
    game_data[JOIN_GAME_MESSAGE_COUNTER_LIMIT] = "5";
    game_data[KEEPALIVE_INTERVAL] = "1.0";
    game_data[LOCAL_GAME_NAME] = "Network Game";
    game_data[PLAYER1_COLOR] = "ffff00";
    game_data[LOAD_MESSAGE_INTERVAL] = "0.2";

    generate_local_id();

    load_settings_from_file();
}

void Resources::generate_local_id()
{
    std::string name = "";
    int length = atoi(game_data[ID_LENGTH].c_str());

    std::cout << "generating id of length " << length << std::endl;

    for (int i = 0; i < length; i++) {
        name += (rand() % 26) + 65;
    }

    std::cout << "generated name: " << name << std::endl;

    game_data[LOCAL_ID] = name;
}

void Resources::load_settings_from_file()
{
    std::ifstream file("settings.cfg", std::ios::in);

    if (!file.is_open()) {
        std::cerr << "Failed to open settings file! Using hardcoded values..." << std::endl;
        return;
    }

    std::cout << "Loading settings from file" << std::endl;

    std::string line;
    std::regex regex_comment("^\\s*#");

    while (std::getline(file, line)) {

        // ignore comments
        if (std::regex_search(line, regex_comment)) {
//            std::cout << "ignoring comment: " << line << std::endl;
            continue;
        }

        auto words = split(line);

        if (words.size() < 2) {
            continue;
        }

        // tie co nechceme dovolit ludom nastavovat vynechame z tohoto zoznamu
        std::map<std::string, EnumGameData> map = {
                {"BROADCAST_INTERVAL",              BROADCAST_INTERVAL},
                {"LOCAL_ID",                        LOCAL_ID},
                {"DEFAULT_PORT",                    DEFAULT_PORT},
                {"LOCAL_PORT",                      LOCAL_PORT},
                {"NETWORK_BUFFER_SIZE",             NETWORK_BUFFER_SIZE},
                {"LOCAL_IP",                        LOCAL_IP},
                {"ID_LENGTH",                       ID_LENGTH},
                {"HOST_CONNECTION_TIMEOUT",         HOST_CONNECTION_TIMEOUT},
                {"WINDOW_WIDTH",                    WINDOW_WIDTH},
                {"WINDOW_HEIGHT",                   WINDOW_HEIGHT},
                {"PLAYER1_NAME",                    PLAYER1_NAME},
                {"PLAYER2_NAME",                    PLAYER2_NAME},
                {"MAX_PLAYERS",                     MAX_PLAYERS},
                {"MAX_CLIENT_AGE",                  MAX_CLIENT_AGE},
                {"JOIN_GAME_MESSAGE_INTERVAL",      JOIN_GAME_MESSAGE_INTERVAL},
                {"JOIN_GAME_MESSAGE_COUNTER_LIMIT", JOIN_GAME_MESSAGE_COUNTER_LIMIT},
                {"KEEPALIVE_INTERVAL",              KEEPALIVE_INTERVAL},
                {"LOCAL_GAME_NAME",                 LOCAL_GAME_NAME},
                {"PLAYER1_COLOR",                   PLAYER1_COLOR},
                {"LOAD_MESSAGE_INTERVAL",           LOAD_MESSAGE_INTERVAL}
        };

        std::cout << words[0] << "\t= " << words[1] << std::endl;
        if (map.find(words[0]) != map.end()) {
            game_data[map[words[0]]] = words[1];
        } else {
            std::cerr << "Unknown entry in settings file!!! (" << words[0] << ")" << std::endl;
        }
    }
}

const sf::Font &Resources::getFont(EnumFonts f)
{
    return fonts[f];
}

std::string &Resources::getGameData(EnumGameData key)
{
    return game_data[key];
}

void Resources::setGameData(EnumGameData key, std::string value)
{
    game_data[key] = value;
}

sf::Uint8 Resources::getColor(char c1, char c2)
{
    if (!((c1 >= '0' && c1 <= '9') || (c1 >= 'A' && c1 <= 'F'))) {
        throw "Invalid hex format";
    }
    if (!((c2 >= '0' && c2 <= '9') || (c2 >= 'A' && c2 <= 'F'))) {
        throw "Invalid hex format";
    }

    return (sf::Uint8) ((c1 >= 'A' && c1 <= 'F' ? ((c1 - 'A' + 10) * 16) : ((c1 - '0') * 16))
                        + (c2 >= 'A' && c2 <= 'F' ? ((c2 - 'A' + 10)) : ((c2 - '0'))));
}

sf::Color Resources::hexToColor(std::string s)
{
    for (char &c : s) { c = toupper(c); }

    if (s.size() < 6) {
        throw "Color string is too short";
    }

    return sf::Color(getColor(s[0], s[1]), getColor(s[2], s[3]), getColor(s[4], s[5]));
}

