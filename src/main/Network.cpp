//
// Created by Duso on 24. 8. 2016.
//

#include "Network.h"
#include "Resources.h"

#include "Message.h"
#include <list>


std::list<Message> Network::queue;
sf::UdpSocket Network::socket;
char Network::status;
char* Network::data;
unsigned short Network::port;
sf::IpAddress Network::address;
size_t Network::received_size;

void Network::init() {
    std::cout << "Network::init()" << std::endl;

    socket.setBlocking(false);

    if ((socket.bind((unsigned short) atoi(Resources::getGameData(DEFAULT_PORT).c_str())) != sf::Socket::Status::Done)){
        std::cerr << "Cannot bind to default port! Binding to random port..." << std::endl;
        socket.bind(sf::Socket::AnyPort);
    }
    Resources::setGameData(LOCAL_PORT,std::to_string(socket.getLocalPort()));
    Resources::setGameData(LOCAL_IP,sf::IpAddress::getLocalAddress().toString());

//    Resources::setGameData(LOCAL_IP, );

    std::cout << "Socket binded to port " << socket.getLocalPort() << std::endl;
    std::cout << "Local ip address is " << sf::IpAddress::getLocalAddress().toString() << std::endl;

    data = new char[(atoi(Resources::getGameData(NETWORK_BUFFER_SIZE).c_str()))];
}

void Network::send(std::string &message, sf::IpAddress dst, unsigned short port) {
    socket.send(message.c_str(),message.size(), dst, port);
}

void Network::send(Message &message) {
    std::cout << "Network::send()" << std::endl;
    std::cout << "address: " << message.addr.toString() << std::endl;
    std::cout << "port     " << message.port << std::endl;
    std::cout << "type:    " << message.getMessageType() << std::endl;
    std::cout << "data:    " << message.data << std::endl;
    std::cout << std::endl;
    send(message.data, message.addr, message.port);
}

void Network::receiveMessages() {
    while((status = socket.receive(data, (size_t) atoi(Resources::getGameData(NETWORK_BUFFER_SIZE).c_str()), received_size, address, port)) != sf::Socket::NotReady){
//        std::cout << "Received message!" << std::endl;
        queue.push_back(Message(std::string(data,received_size),address,port));
    }
}

bool Network::pollMessage(Message &msg) {
    if (queue.size() == 0){
        return false;
    }
    msg = *queue.begin();
    queue.pop_front();
    return true;
}

void Network::flushMessages() {
    while((status = socket.receive(data, (size_t) atoi(Resources::getGameData(NETWORK_BUFFER_SIZE).c_str()), received_size, address, port)) != sf::Socket::NotReady);
    queue.clear();
//    receiveMessages();
//    queue.clear();
}
