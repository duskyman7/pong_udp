//
// Created by Duso on 25. 8. 2016.
//

#ifndef PONG_UDP_MESSAGE_H
#define PONG_UDP_MESSAGE_H

#include <string>
#include <list>
#include <iostream>
#include "SFML/Network/UdpSocket.hpp"
#include "SFML/Network/IpAddress.hpp"
#include "../thirdparty/json.hpp"

class Player;
class Server;

using Json = nlohmann::json;

enum EnumMessageType
{
    BAD_MESSAGE = -1,
    SERVER_BROADCAST_INVITE = 0,
    CLIENT_REQUEST_JOIN,
    HOST_KEEPALIVE,
    SERVER_JOIN_ACCEPT,
    SERVER_LOBBY_DATA,
    SERVER_GAME_LOADING,
    CLIENT_GAME_LOADING,
    SERVER_GAME_DATA,
    CLIENT_GAME_DATA
};

class MessageJsonFields
{
public:
    static std::string ID;
    static std::string GAME_NAME;
    static std::string PLAYERS;
    static std::string FRAME;
    static std::string PLAYER_NAME;
    static std::string COLOR;
    static std::string IS_SERVER;
    static std::string LOAD;
    static std::string PADDLE_L;
    static std::string PADDLE_R;
    static std::string BALL;
};

class Message
{
public:
    Message()
    {}

    Message(sf::IpAddress address, unsigned short port) :
            addr(address),
            port(port)
    {}

    Message(std::string data, sf::IpAddress address, unsigned short port) :
            data(data),
            addr(address),
            port(port)
    {}

    std::string data;
    sf::IpAddress addr;
    unsigned short port;

    std::string getMessageType();

    static std::string getMessageType(EnumMessageType type);

    EnumMessageType getType()
    { return getType(data); }

    static EnumMessageType getType(std::string &msg);

    std::string getSourceId();

    std::string getDestinationId();

    Json getJson();

    uint32_t getFrame();

    void createBroadcastMessage(std::string &src_id, std::string &game_name);

    void createJoinGameMessage();

    void createServerJoinAcceptMessage(std::vector<Player> &players, int frame, std::string &dst_id, Server &server);

    void createKeepAliveMessage(uint32_t frame);

    void createGameStartMessage(std::string &src_id, std::string &dst_id);

    void createServerLobbyDataMessage(std::vector<Player> &players, uint32_t frame, std::string &dst_id,
                                      Server &server);

    static std::string intToBytes(uint32_t x);

    static uint32_t bytesToInt(std::string s);

    void createLoadingMessage(Json json, bool isSrcServer);
};

#endif //PONG_UDP_MESSAGE_H
