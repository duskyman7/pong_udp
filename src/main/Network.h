//
// Created by Duso on 24. 8. 2016.
//

#ifndef PONG_UDP_NETWORK_H
#define PONG_UDP_NETWORK_H


#include <string>
#include <list>
#include <iostream>
#include "SFML/Network/UdpSocket.hpp"
#include "SFML/Network/IpAddress.hpp"
#include "Message.h"

class Network {
public:
    static void init();
    static void send(std::string &message, sf::IpAddress dst, unsigned short port);
    static void receiveMessages();

    static bool pollMessage(Message &msg);

    static void flushMessages();

    static void send(Message &message);

private:
    static std::list<Message> queue;
    static sf::UdpSocket socket;
    static char* data;
    static unsigned short port;
    static sf::IpAddress address;
    static size_t received_size;
    static char status;
};


#endif //PONG_UDP_NETWORK_H
