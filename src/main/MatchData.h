//
// Created by Duso on 7. 9. 2016.
//

#ifndef PONG_UDP_MATCHDATA_H
#define PONG_UDP_MATCHDATA_H

#include "../data/Server.h"

class MatchData {

public:
    static void initFromJoinGame(Json &json, Server *pServer);

    static std::vector<Player> players;
    static std::map<std::string,std::string> aliases;
    static Server server;
    static uint32_t frame;
    static uint32_t last_change_frame;
};

#endif //PONG_UDP_MATCHDATA_H
