//
// Created by Duso on 16. 8. 2016.
//

#include "StateManager.h"
#include "../states/MainMenu.h"
#include "../states/LobbyClient.h"
#include "../states/LobbyServer.h"
#include "../states/JoinGame.h"
#include "../states/LoadGame.h"
#include "../states/GameServer.h"
#include "../states/GameClient.h"

std::map<EnumStates, std::string> EnumStatesNames = {
        {MAIN_MENU,    "MainMenu"},
        {LOBBY_SERVER, "LobbyServer"},
        {LOBBY_CLIENT, "LobbyClient"},
        {JOIN_GAME,    "JoinGame"},
        {GAME_SERVER,  "GameServer"},
        {GAME_CLIENT,  "GameClient"},
        {LOAD_GAME,    "LoadGame"}

};

void StateManager::run()
{
    std::cout << "StateManager::run()" << std::endl;
    m_running = true;
    m_clock.restart();

    while (m_running) {

        if (m_next_state != nullptr) {
            m_current_state->unload();
            m_current_state = m_next_state;
            m_current_state->load();
            m_next_state = nullptr;
        }

        if ((m_elapsed = m_clock.getElapsedTime().asSeconds()) >= m_tpf) {
            m_clock.restart();
            m_window.clear();
            m_current_state->input();
            m_current_state->update(m_elapsed);
            m_current_state->render();
            postDraw();
            m_elapsed -= m_tpf;
        }
    }

    m_window.close();
}

void StateManager::init()
{
    std::cout << "StateManager::init()" << std::endl;
    m_window.create(sf::VideoMode{(unsigned int) atoi(Resources::getGameData(WINDOW_WIDTH).c_str()),
                                  (unsigned int) atoi(Resources::getGameData(WINDOW_HEIGHT).c_str())},
                    "Pongac");
    m_show_fps = true;
    m_tpf = 1.0f / 60.0f;
    prepareFpsCounter();
    fillStates();
    m_current_state = m_state_map[MAIN_MENU];
}

void StateManager::postDraw()
{
    if (m_show_fps) {
        m_label_fps.setString(std::to_string(1.0f / m_elapsed));
        m_window.draw(m_label_fps);
        m_window.display();
    }
}

void StateManager::stop()
{
    m_running = false;
}

void StateManager::changeState(EnumStates state)
{
    std::cout << "Changing state to " << EnumStatesNames[state] << std::endl;
    m_next_state = m_state_map[state];
}

void StateManager::fillStates()
{
    m_state_map[MAIN_MENU] = new MainMenu(this);
    m_state_map[LOBBY_SERVER] = new LobbyServer(this);
    m_state_map[LOBBY_CLIENT] = new LobbyClient(this);
    m_state_map[JOIN_GAME] = new JoinGame(this);
    m_state_map[LOAD_GAME] = new LoadGame(this);
    m_state_map[GAME_SERVER] = new GameServer(this);
    m_state_map[GAME_CLIENT] = new GameClient(this);
}

void StateManager::prepareFpsCounter()
{
    m_label_fps.setFont(Resources::getFont(FONT1));
    m_label_fps.setPosition(0, 0);
    m_label_fps.setCharacterSize(20);
    m_label_fps.setFillColor(sf::Color::Yellow);
}


