#include <iostream>
#include "StateManager.h"
#include "Resources.h"
#include "Network.h"

using namespace std;

int main()
{
    std::cout << "startujem" << std::endl;

    srand(time(nullptr));

    StateManager sm;

    Resources::init();
    Network::init();

    sm.init();
    sm.run();

    return 0;
}