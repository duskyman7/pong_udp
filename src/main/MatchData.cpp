//
// Created by Duso on 10. 9. 2016.
//

#include "MatchData.h"
#include "../data/Player.h"

Server MatchData::server;
uint32_t MatchData::frame;
uint32_t MatchData::last_change_frame;
std::vector<Player> MatchData::players;
std::map<std::string,std::string> MatchData::aliases;

void MatchData::initFromJoinGame(Json &json, Server *pServer)
{
    std::cout << "MatchData::initFromJoinGame()" << std::endl;

    typedef MessageJsonFields f;

    server = *pServer;
    frame = json[f::FRAME];

    players.clear();

    if (json.find(f::PLAYERS) == json.end()){
        throw "Could not find players info in the message";
    }

    for (Json player: json[f::PLAYERS]) {
        Player p;
        p.name = player[f::PLAYER_NAME];
        p.color = player[f::COLOR];

        if (player.find(f::IS_SERVER) != player.end()){
            p.is_server = true;
        }

        if (player.find(f::ID) != player.end()){
            p.id = player[f::ID];
        }

        players.push_back(p);
    }

    std::cout << "connected players: " << std::endl;
    for (Player &p : players) {
        std::cout << p.name << std::endl;
    }
    std::cout << std::endl;
}
