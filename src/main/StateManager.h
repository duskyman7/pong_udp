//
// Created by Duso on 16. 8. 2016.
//

#ifndef PONG_UDP_STATEMANAGER_H
#define PONG_UDP_STATEMANAGER_H

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>
#include <map>
#include <iostream>

class State;

//typedef std::shared_ptr<State> StatePtr;

enum EnumStates
{
    MAIN_MENU,
    LOBBY_SERVER,
    LOBBY_CLIENT,
    JOIN_GAME,
    GAME_SERVER,
    GAME_CLIENT,
    LOAD_GAME
};

class StateManager
{
public:
    StateManager()
    {
        std::cout << "StateManager::StateManager()" << std::endl;
    };

    void run();

    void init();

    sf::RenderWindow m_window;

    void stop();

    void changeState(EnumStates state);

private:
    sf::Text m_label_fps;
    sf::Clock m_clock;
    std::map<EnumStates, State *> m_state_map;
    State *m_current_state = nullptr;
    State *m_next_state = nullptr;
    bool m_running;
    bool m_show_fps;
    float m_elapsed;
    float m_tpf;

    void postDraw();

    void fillStates();

    void prepareFpsCounter();
};


#endif //PONG_UDP_STATEMANAGER_H
