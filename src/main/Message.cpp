//
// Created by Duso on 25. 8. 2016.
//

#include <map>
#include "Message.h"
#include "Resources.h"

#include "../data/Server.h"
#include "../data/Player.h"
#include "MatchData.h"

std::string MessageJsonFields::ID = "ID";
std::string MessageJsonFields::GAME_NAME = "GAME_NAME";
std::string MessageJsonFields::PLAYERS = "PLAYERS";
std::string MessageJsonFields::FRAME = "FRAME";
std::string MessageJsonFields::PLAYER_NAME = "PLAYER_NAME";
std::string MessageJsonFields::COLOR = "COLOR";
std::string MessageJsonFields::IS_SERVER = "IS_SERVER";
std::string MessageJsonFields::LOAD = "LOAD";
std::string MessageJsonFields::PADDLE_L = "PADDLE_L";
std::string MessageJsonFields::PADDLE_R = "PADDLE_R";
std::string MessageJsonFields::BALL = "BALL";

std::string Message::getMessageType()
{
    return getMessageType(getType());
}

std::string Message::getMessageType(EnumMessageType type)
{
    switch (type) {
        case BAD_MESSAGE:
            return "BAD_MESSAGE";
        case SERVER_BROADCAST_INVITE:
            return "SERVER_BROADCAST_INVITE";
        case CLIENT_REQUEST_JOIN:
            return "CLIENT_REQUEST_JOIN";
        case HOST_KEEPALIVE:
            return "HOST_KEEPALIVE";
        case SERVER_JOIN_ACCEPT:
            return "SERVER_JOIN_ACCEPT";
        case SERVER_LOBBY_DATA:
            return "SERVER_LOBBY_DATA";
        case SERVER_GAME_LOADING:
            return "SERVER_GAME_LOADING";
        case CLIENT_GAME_LOADING:
            return "CLIENT_GAME_LOADING";
        case SERVER_GAME_DATA:
            return "SERVER_GAME_DATA";
        case CLIENT_GAME_DATA:
            return "CLIENT_GAME_DATA";
    }

    // NEDAVAT SEM DEFAULT - radsej nech papuluje -Wall

    std::cerr << "Message::GetMessageType(): UNCOVERED TYPE RECEIVED!!!" << std::endl;
    return "UNKNOWN";
}

EnumMessageType Message::getType(std::string &msg)
{
    if (msg.size() == 0) {
        std::cerr << "Network::getType(): RECEIVED MESSAGE WITH ZERO LENGTH!" << std::endl;
        return BAD_MESSAGE;
    }
    return (EnumMessageType) msg[0];
}

void Message::createBroadcastMessage(std::string &src_id, std::string &game_name)
{
    Json json;

    typedef MessageJsonFields f;

    json[f::ID] = src_id;
    json[f::GAME_NAME] = game_name;

    this->data.clear();
    this->data += (char) (SERVER_BROADCAST_INVITE);
    this->data += json.dump();

    std::cout << "Message::createBroadcastMessage: " << SERVER_BROADCAST_INVITE << src_id << std::endl;
}

void Message::createJoinGameMessage()
{
    typedef MessageJsonFields f;

    Json json;

    json[f::PLAYER_NAME] = Resources::getGameData(PLAYER1_NAME);
    json[f::FRAME] = 0;
    json[f::ID] = Resources::getGameData(LOCAL_ID);
    json[f::COLOR] = Resources::getGameData(PLAYER1_COLOR);

    this->data.clear();
    this->data += (char) (CLIENT_REQUEST_JOIN);
    this->data += json.dump();
}

void
Message::createServerJoinAcceptMessage(std::vector<Player> &players, int frame, std::string &dst_id, Server &server)
{
    typedef MessageJsonFields f;

    Json json;

    json[f::ID] = Resources::getGameData(LOCAL_ID);
    json[f::PLAYERS] = Json::array();
    json[f::FRAME] = frame;

    for (Player &p: players) {
        Json pp;
        pp[f::PLAYER_NAME] = p.name;
        pp[f::COLOR] = p.color;

        if (p.id == server.id) {
            pp[f::IS_SERVER] = true;
        }

        // send him his own id or alias
        if ((p.id == dst_id) || (p.id == MatchData::server.id)) {
            pp[f::ID] = p.id;
        }
        else{
            pp[f::ID] = MatchData::aliases[p.id];
        }

        json[f::PLAYERS].push_back(pp);
    }

    this->data.clear();
    this->data += (char) (SERVER_JOIN_ACCEPT);
    this->data += json.dump();

    std::cout << "LobbyData:" << std::endl;
    std::cout << json.dump(2) << std::endl << std::endl;
}

void Message::createServerLobbyDataMessage(std::vector<Player> &players, uint32_t frame, std::string &dst_id,
                                           Server &server)
{
    createServerJoinAcceptMessage(players, frame, dst_id, server);
    this->data[0] = (char) (SERVER_LOBBY_DATA);
}

void Message::createKeepAliveMessage(uint32_t frame)
{
    this->data.clear();

    Json json;
    typedef MessageJsonFields f;
    json[f::FRAME] = intToBytes(frame);
    json[f::ID] = Resources::getGameData(LOCAL_ID);

    this->data += (char) (HOST_KEEPALIVE);
    this->data += json.dump();
}

void Message::createGameStartMessage(std::string &src_id, std::string &dst_id)
{
    this->data.clear();
    this->data += (char) (SERVER_GAME_LOADING);
    this->data += src_id;
    this->data += dst_id;
}

void Message::createLoadingMessage(Json json, bool isSrcServer)
{
    this->data.clear();
    this->data += (char) ((isSrcServer) ? (SERVER_GAME_LOADING) : (CLIENT_GAME_LOADING));
    this->data += json.dump();
}

std::string Message::getSourceId()
{

    unsigned int id_length = (unsigned int) atoi(Resources::getGameData(ID_LENGTH).c_str());

    if (data.size() <= id_length) {
        return "NEBUDE SA DAT!!!";
    }

    return data.substr(1, id_length);
}

std::string Message::getDestinationId()
{

    unsigned int id_length = (unsigned int) atoi(Resources::getGameData(ID_LENGTH).c_str());

    if (data.size() <= id_length * 2) {
        throw "NEBUDE SA DAT!!!";
//        return "NEBUDE SA DAT!!!";
    }

    return data.substr(1 + id_length, id_length);
}

Json Message::getJson()
{
//    std::cout << "Message::getJson()" << std::endl;
    /*switch (getType()){
        case SERVER_BROADCAST_INVITE:
        std::cout << "creating json from string: \"" << data.substr(1) << "\"" << std::endl;
            return Json::parse(data.substr(1));
        case CLIENT_REQUEST_JOIN:
            return Json::parse(data.substr(1+16+16));
        case SERVER_JOIN_ACCEPT:
        case SERVER_LOBBY_DATA:
            return Json::parse(data.substr(1+16+16+4));

        default:
        std::cerr << "Message::getJson(): REQUESTING JSON FROM BAD / UNKNOWN MESSAGE TYPE!!!" << std::endl;
    }
    return Json();
     */
    return Json::parse(data.substr(1));
}

uint32_t Message::getFrame()
{
    if (getType() == HOST_KEEPALIVE || getType() == SERVER_JOIN_ACCEPT) {
        if (data.size() >= 37) {
            return bytesToInt(data.substr(33, 4));
        } else {
            std::cerr << "Message::getFrame(): TOO SMALL MESSAGE TO PARSE FRAME!!!" << std::endl;
        }
    } else {
        std::cerr << "Message::getFrame(): UNKNOWN MESSAGE TYPE!!!" << std::endl;
    }

    return 0;
}

std::string Message::intToBytes(uint32_t x)
{
    std::string bytes;
    bytes += (unsigned char) (x & 255);
    bytes += (unsigned char) (x >> 8 & 255);
    bytes += (unsigned char) (x >> 16 & 255);
    bytes += (unsigned char) (x >> 24 & 255);
    return bytes;
}

uint32_t Message::bytesToInt(std::string s)
{
    if (s.size() != 4) {
        std::cerr << "Message::bytesToInt(): BAD STRING PASSED!!!" << std::endl;
        return 0;
    }
    return ((uint32_t) ((unsigned char) s[0]))
           + ((uint32_t) ((unsigned char) s[1]) << 8)
           + ((uint32_t) ((unsigned char) s[2]) << 16)
           + ((uint32_t) ((unsigned char) s[3]) << 24);
}








