//
// Created by Duso on 16. 8. 2016.
//

#ifndef PONG_UDP_RESOURCES_H
#define PONG_UDP_RESOURCES_H


#include <SFML/Graphics/Font.hpp>
#include <iostream>
#include <map>
#include <regex>

enum EnumFonts
{
    FONT1
};

enum EnumGameData
{
    BROADCAST_INTERVAL,
    DEFAULT_PORT,
    HOST_CONNECTION_TIMEOUT,
    ID_LENGTH,
    JOIN_GAME_MESSAGE_COUNTER_LIMIT,
    JOIN_GAME_MESSAGE_INTERVAL,
    KEEPALIVE_INTERVAL,
    LOAD_MESSAGE_INTERVAL,
    LOCAL_GAME_NAME,
    LOCAL_ID,
    LOCAL_IP,
    LOCAL_PORT,
    MAX_CLIENT_AGE,
    MAX_PLAYERS,
    NETWORK_BUFFER_SIZE,
    PLAYER1_COLOR,
    PLAYER1_NAME,
    PLAYER2_NAME,
    WINDOW_HEIGHT,
    WINDOW_WIDTH
};

class Resources
{
public:

    static void init();

    static const sf::Font &getFont(EnumFonts f);

    static std::string &getGameData(EnumGameData key);

    static void setGameData(EnumGameData key, std::string value);

    static std::vector<std::string> split(std::string s);

    static sf::Uint8 getColor(char c1, char c2);

    static sf::Color hexToColor(std::string s);

private:
    static std::map<EnumFonts, sf::Font> fonts;
    static std::map<EnumGameData, std::string> game_data;

    static void generate_local_id();

    static void load_settings_from_file();


};


#endif //PONG_UDP_RESOURCES_H
