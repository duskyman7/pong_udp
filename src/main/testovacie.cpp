//
// Created by Duso on 25. 8. 2016.
//

#include <iostream>
#include <vector>
#include <regex>
#include <SFML/Window/VideoMode.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include "../thirdparty/json.hpp"
#include "Message.h"
#include "Resources.h"

using Json = nlohmann::json;

std::vector<std::string> split(std::string s, std::regex r){
    std::sregex_iterator it(s.begin(), s.end(), r);
    std::sregex_iterator end;
    std::vector<std::string> vec;

    while(it != end){
        vec.push_back(it->str());
        it++;
    }

    return vec;
}

void a(){
    std::string s = "123456789";

    std::cout << s.substr(2,4) << std::endl;

    std::cout << s.substr(5) << std::endl;

    std::cout << s.substr(1, 7) << std::endl;
}

void b(){
    std::vector<int> vector = {5,3,4,1,2,6,2,7};

    for (int i: vector){
        std::cout << i << " ";
    }
    std::cout << std::endl;

    auto it = vector.begin();

    while(it != vector.end()){
        if ((*it) < 4){
            vector.erase(it);
        }
        else{
            it++;
        }
    }

    std::cout << std::endl;

    for (int i: vector){
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

void c() {
    std::string text = "{\"a\":42,\"b\":[1,2,3],\"c\":{\"name\":\"zebrak\",\"age\":9}}";
    Json json = Json::parse(text);
    std::cout << json.dump().size() << std::endl << std::endl;

    for (auto x: json){
        std::cout << x << std::endl;
    }

    std::cout << std::endl;

    if (json.find("a") != json.end()){
        std::cout << json["a"] << std::endl;
    }
    else{
        std::cout << "fail" << std::endl;
    }
}

void d(){

    unsigned char a[4];

    int *x = (int*)a;

    (*x) = 2958381294;

    std::cout << (unsigned int)a[0] << " " << (unsigned int)a[1] << " " << (unsigned int)a[2] << " " << (unsigned int)a[3];

    std::cout << std::endl << std::endl;


    std::vector<unsigned int> numbers = {1,3,8,127,128,255,256,480,1024,2176,65540,1000000,128459125,2958381294};

    for(unsigned int x: numbers){
        std::cout << x << " = ";
        std::cout << (x & 255) << " " << (x >> 8 & 255) << " " << (x >> 16 & 255) << " " << (x >> 24 & 255) << std::endl;
    }
}

void e(){
    std::vector<unsigned int> numbers = {1,3,8,127,128,255,256,480,1024,2176,65540,1000000,128459125,2958381294};

    for (auto x: numbers){
        for (unsigned char c: Message::intToBytes(x)){
            std::cout << (unsigned int)c << " ";
        }
        std::cout << " = " << Message::bytesToInt(Message::intToBytes(x)) << std::endl;
    }

    std::cout << Message::bytesToInt({192,168,0,1}) << " = ";
    for (unsigned char c: Message::intToBytes(Message::bytesToInt({192,168,0,1}))){
        std::cout << (unsigned int)c << " ";
    }
    std::cout << std::endl;
}

void f(){
    Resources::setGameData(LOCAL_ID,"qwertyuiopasdfgh");
    Message msg(sf::IpAddress::LocalHost,0);
    std::string s = "abcdefghijklmnop";
//    Json json = Json::parse("{\"test\":[\"abc\",\"def\"],\"foo\":{\"x\":192,\"y\":256}}");
    Json json;

    json = Json::parse("{\"Zebrak\": 123, \"QWER\": \"aerh\"}");

    auto it = json.begin();

    while (it != json.end()){
        std::cout << it.key() << " : " << it.value() << std::endl;
        it++;
    }


    //json["zebrak"] = "123";

//    msg.createServerJoinAcceptMessage(s);
    std::cout << "message data: " << msg.data << std::endl;
    std::cout << msg.getJson().dump(2) << std::endl;
}

void g(){
    std::vector<std::regex> regexes = {
        std::regex("\\S+"),
        std::regex("[^\\s]+")
    };

    std::sregex_iterator end;

    std::string veta = "asdf qwer. ?zxcv    _what \taaa \nmmmm";

    for(std::regex& rx: regexes){
        for (std::sregex_iterator it(veta.begin(), veta.end(), rx); it != end; it++){
            std::cout << it->str() << std::endl;
        }

        std::cout << std::endl;
    }
}

void h(){
    auto words = split("   asdf Huuu qwer zxcv \t\nnananana\t Waaaa  _bbb __trttr XXX Olaa ?ahaaa !nene! tarat!", std::regex("\\S+"));
    for (std::string &word: words){
        std::cout << word << std::endl;
    }
}

void i(){
    std::vector<std::string> lines = {
            "\t     #mena hracov",
            "PLAYER1_NAME                 Alica v krajine zazrakov",
            "PLAYER2_NAME Barbar",
            "       #dalsi pokusny koment",
            "    \t\t   \t#aj tento s tabulatorom na zaciatku"
    };

    std::regex r("^\\s*#");

    std::cout << "regex_match" << std::endl;

    for (std::string &s: lines){
        if (std::regex_match(s, r)){
            std::cout << s << std::endl;
        }
    }

    std::cout << "regex_search" << std::endl;

    for (std::string &s: lines){
        if (std::regex_search(s, r)){
            std::cout << s << std::endl;
        }
    }

    std::cout << "regex iterator" << std::endl;

    r = std::regex("\\S+");

    for(std::string &s: lines){
        std::sregex_iterator it(s.begin(), s.end(), r);
        std::sregex_iterator end;

        int length;

        while(it != end){
            length++;
            it++;
        }

        it = std::sregex_iterator(s.begin(), s.end(), r);

        if (length >= 2){
//            std::cout << it->match_results().size() << " ";
//            std::cout << it->position() << " ";
//            std::cout << length << " " << it->str() << " ";
            std::cout << it->str() << " ";
            it++;
            std::cout << s.substr(it->position()) << " ";
//            std::cout << it->position() << std::endl;
        }

        std::smatch matches;
        std::regex_search(s,matches,r);
        std::cout << std::endl;
        std::cout << std::endl;
        std::cout << matches.size() << std::endl;
        std::cout << std::endl;
        std::cout << std::endl;
    }


}

void j(){
    sf::RenderWindow w(sf::VideoMode(200,200),"Test TextEntered eventu");

    sf::Event e;

    while(w.isOpen()){
        while(w.pollEvent(e)){
            if (e.type == sf::Event::TextEntered){
                std::cout << e.text.unicode << " = " << (char)(e.text.unicode) << std::endl;
            }
            if (e.type == sf::Event::Closed){
                w.close();
            }
        }
        w.clear();
        w.display();
    }
}

void k()
{
    std::string a = "asdf";

    std::string b = "qwer";

    std::string c = "asdf";

    std::cout << (a == b) << std::endl;
    std::cout << (b == a) << std::endl;
    std::cout << (a == c) << std::endl;
    std::cout << (c == a) << std::endl;
    std::cout << a.compare(b) << std::endl;
    std::cout << b.compare(a) << std::endl;
    std::cout << a.compare(c) << std::endl;
}

void l(){
    std::string s = "[\"GOALWJRBKFAPCOTO\",\"duskova hra\"]";
    Json json = Json::parse(s);
    std::cout << json[0] << std::endl;
    std::cout << json[1] << std::endl;

    json.clear();
    json[3] = "asdf";
    json[6] = "qwer";

    std::cout << json.dump() << std::endl;
}

void m(){
    sf::Color c = Resources::hexToColor("ff10a2");
    std::cout << (int)c.r << " " << (int)c.g << " " << (int)c.b << std::endl;
    c = Resources::hexToColor("000000");
    std::cout << (int)c.r << " " << (int)c.g << " " << (int)c.b << std::endl;
    c = Resources::hexToColor("ffffff");
    std::cout << (int)c.r << " " << (int)c.g << " " << (int)c.b << std::endl;
    c = Resources::hexToColor("123456");
    std::cout << (int)c.r << " " << (int)c.g << " " << (int)c.b << std::endl;
    c = Resources::hexToColor("abcdef");
    std::cout << (int)c.r << " " << (int)c.g << " " << (int)c.b << std::endl;
}

void fn(std::string slovo, std::vector<char> pismenka){

    if (pismenka.size() == 1){
        std::cout << slovo << std::endl;
        return;
    }

    for (char c: pismenka){
        std::vector<char> tmp(pismenka);

        auto it = tmp.begin();
        while(it != tmp.end()){
            if (*it == c){
                tmp.erase(it);
                break;
            }
            it++;
        }

        std::string tmp2 = slovo + c;

        if (tmp2.size() == 2){
            tmp2 += 't';
        }
        else if (tmp2.size() == 4){
            tmp2 += 'h';
        }

        fn(tmp2, tmp);
    }
}

void n(){
    fn("", {'c','p','r','a','e','i'});
}

int main(){
    //a();
    //b();
//    c();
//    d();
//
//    std::cout << std::endl;
//
//    e();
//    f();
//    g();
//    h();
//    i();
//    j();
/*
    sf::IpAddress addr("555.555.555.555");

    std::cout << addr.toString() << std::endl;

    if (addr == sf::IpAddress::None){
        std::cout << "bad IP Address" << std::endl;
    }*/
//    k();
//    l();
//    m();
//    n();
    return 0;
}
